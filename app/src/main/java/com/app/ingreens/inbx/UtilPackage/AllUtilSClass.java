package com.app.ingreens.inbx.UtilPackage;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.ingreens.inbx.R;

import java.util.ArrayList;

/**
 * Created by root on 7/18/16.
 */
public class AllUtilSClass {

    public class CheckOutViewHolder {
        public ImageView imv_product_image;
        public TextView tv_title;
        public ImageButton btn_minus, btn_plus, btn_trash;
        public TextView tv_count, tv_price;
        public int count=0;
    }

    public class OrderDetailsViewHolder {
        public LinearLayout ll_holder;
        public ImageView imv_product_image;
        public TextView tv_title,tv_qty, tv_price;
    }

    public class AdvanceSearchPriceViewHolder {
        public CheckBox chk_filter;
    }

    public class Category{
        public String id;
        public String name;
        public String thumb;
        public String image;
    }

    public class SubCategory{
        public String id;
        public String name;
        public String image;
    }
    public class MainSlideImage{
        public String title;
        public String image;
    }
    public class HotDeals{
        public String id;
        public String image_url;
        public String name;
        public String tierPrices="";
        public String spprice="";
        public String rgprice="";
        public String stock_status;
        public String stock;
        public String url;
    }

    public class MostView{
        public String id;
        public String image_url;
        public String name;
        public String tierPrices="";
        public String spprice="";
        public String rgprice="";
        public String stock_status;
        public String stock;
        public String url;
    }

    public class ProductList{
        public String product_id;
        public String image_url;
        public String name;
        public String position;
        public String rgprice="";
        public String set;
        public String sku;
        public String spprice="";
        public String status;
        public String stock;
        public String tierPrices;
        public String stock_status;
        public String brand;
        public String fltprice;
    }

    public class ProductDetailsImageUrl{
        public String position;
        public String image;
    }

    public class ProductDetailsSimilarProductImageUrl{
        public String id;
        public String imageurl;
        public String name;
        public String rgprice;
        public String spprice;
        public String url;
    }

    public class ProductDetailsTierPrices{
        public String info;
    }

    public class ProductDetails{
        public String product_id;
        public String sku;
        public String set;
        public String type_id;
        public String name;
        public String description;
        public String short_description;
        public String hot_deal;
        public String spprice="";
        public String rgprice="";
        public String status;
        public String stock;
        public String stock_status;
        public String type;
        public String image_url;

        public ArrayList<ProductDetailsImageUrl> al_image_url=new ArrayList<ProductDetailsImageUrl>();
        public ArrayList<ProductDetailsSimilarProductImageUrl> al_similar_product=new ArrayList<ProductDetailsSimilarProductImageUrl>();
        public ArrayList<ProductDetailsSimilarProductImageUrl> al_upsell_product=new ArrayList<ProductDetailsSimilarProductImageUrl>();
        public ArrayList<ProductDetailsTierPrices> al_tier_price=new ArrayList<ProductDetailsTierPrices>();

    }

    public class MyCartItems{
        public String productid;
        public String imageurl;
        public String name;
        public String price;
        public String qty;
    }

    public class MyCartDetails{
        public String grandTotal;
        public String couponCode;
        public String discount;
        public String totalQty;
        public String subtotal;
        public String shipping_info;
       public ArrayList<MyCartItems> al_cart_items=new ArrayList<MyCartItems>();
    }

    public class TrackOrder{
        public String id;
        public String orderDate;
        public String orderId;
        public String orderShip;
        public String orderStatus;
        public String orderTotal;
    }

    public class NoSpacingItemDecoreation extends RecyclerView.ItemDecoration {
        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = 0;
            outRect.left = 0;
            outRect.right = 0;
            outRect.top = 0;
        }
    }
    public class OrderDetailsList{
        public String price;
        public String productId;
        public String productName;
        public String qty;
        public String image_url;
        public String sku;
        public String totalPrice;
    }

    public class MyOrderDetails{
        public String orderDate;
        public String orderShipFirstNm;
        public String orderShipLastNm;
        public String orderShipPostCode;
        public String orderShipStrt;
        public String orderShipTel;
        public String orderShipComp="NA";
        public String orderShipCty;
        public String shipping;
        public String stotal;
        public String dtotal;
        public String gtotal;
        public ArrayList<OrderDetailsList> al_order_items=new ArrayList<OrderDetailsList>();
    }

    public class Brand{
        public String id;
        public String value;
        public int check=0;
    }
    public class Price{
        public String min="",max="";
        public String value;
        public int check=0;
    }
}
