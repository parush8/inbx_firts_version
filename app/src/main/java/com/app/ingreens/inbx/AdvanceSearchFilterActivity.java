package com.app.ingreens.inbx;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilAdapters.AdvanceSearchBrandAdapter;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 8/18/16.
 */
public class AdvanceSearchFilterActivity extends MyCustomActivity implements View.OnClickListener {
    //////////////////// UI VARIABLES ///////////////////
    private CoordinatorLayout overview_coordinator_layout;
    private ImageButton btn_delete;
    private Button btn_clear, btn_brand, btn_price, btn_apply;
    public RecyclerView rv;
    private LinearLayout ll_holder;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    private boolean resume;
    private String cat_id;
    private InBxApplication mApp;
    private ArrayList<AllUtilSClass.AdvanceSearchPriceViewHolder> al_viewholder;
    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(AdvanceSearchFilterActivity.this));
        setContentView(R.layout.advance_search_filter_activity);

        btn_delete = (ImageButton) findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(this);

        overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            mApp = ((InBxApplication) getApplicationContext());
            cat_id = getIntent().getExtras().getString("cat_id");
            if (cat_id.equalsIgnoreCase(getSharedPreferences("login", 0).getString("brand_cat_id", ""))) {
                if (((InBxApplication) AdvanceSearchFilterActivity.this.getApplication()).al_brand_list.size() > 0) {
                    setUI();
                } else {
                    getBrandAndPrice(cat_id);
                }
            } else {
                getBrandAndPrice(cat_id);
            }
        }
        resume = true;
    }

    private void setUI() {

        btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(this);

        btn_brand = (Button) findViewById(R.id.btn_brand);
        btn_brand.setOnClickListener(this);

        btn_price = (Button) findViewById(R.id.btn_price);
        btn_price.setOnClickListener(this);

        btn_apply = (Button) findViewById(R.id.btn_apply);
        btn_apply.setOnClickListener(this);

        ll_holder= (LinearLayout) findViewById(R.id.ll_holder);

        rv = (RecyclerView) findViewById(R.id.rv);
        AdvanceSearchBrandAdapter aba = new AdvanceSearchBrandAdapter(AdvanceSearchFilterActivity.this, ((InBxApplication) AdvanceSearchFilterActivity.this.getApplication()).al_brand_list);
        LinearLayoutManager lLayout = new LinearLayoutManager(AdvanceSearchFilterActivity.this);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(lLayout);
        rv.setAdapter(aba);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    private void loadMenuItem() {
        ll_holder.removeAllViews();
        al_viewholder = new ArrayList<AllUtilSClass.AdvanceSearchPriceViewHolder>();
        for (int i = 0; i < mApp.al_price_list.size(); i++) {
            ll_holder.addView(addMenuView(i));
        }
        System.out.println(" TOTAL ");
    }


    public View addMenuView(final int position) {
        View v;
        v = LayoutInflater.from(AdvanceSearchFilterActivity.this).inflate(R.layout.filter_list_cell, null);
        AllUtilSClass a = new AllUtilSClass();
        AllUtilSClass.AdvanceSearchPriceViewHolder cvh = a.new AdvanceSearchPriceViewHolder();

        cvh.chk_filter=(CheckBox)v.findViewById(R.id.chk_filter) ;
        cvh.chk_filter.setText(mApp.al_price_list.get(position).value);

        if(mApp.al_price_list.get(position).check==1){
            cvh.chk_filter.setChecked(true);
            System.out.println(" SELECTED PRICE "+mApp.al_price_list.get(position).value);
        }else{
            cvh.chk_filter.setChecked(false);
        }

        cvh.chk_filter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    for(int i=0;i<mApp.al_price_list.size();i++){
                        mApp.al_price_list.get(i).check=0;
                        al_viewholder.get(i).chk_filter.setChecked(false);
                    }
                    mApp.al_price_list.get(position).check=1;
                    al_viewholder.get(position).chk_filter.setChecked(true);
                }else{
                    for(int i=0;i<mApp.al_price_list.size();i++){
                        mApp.al_price_list.get(position).check=0;
                        al_viewholder.get(i).chk_filter.setChecked(false);
                    }
                }
            }
        });

        al_viewholder.add(cvh);
        return v;
    }

    @Override
    public void onClick(View v) {
        final InBxApplication mApp = ((InBxApplication) AdvanceSearchFilterActivity.this.getApplicationContext());
        switch (v.getId()) {
            case R.id.btn_clear:
                for (int i = 0; i < mApp.al_brand_list.size(); i++) {
                    mApp.al_brand_list.get(i).check=0;
                }
                rv.getAdapter().notifyDataSetChanged();
                for(int i=0;i<mApp.al_price_list.size();i++){
                    mApp.al_price_list.get(i).check=0;
                }
                loadMenuItem();
                break;
            case R.id.btn_delete:
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
                break;
            case R.id.btn_brand:
                btn_brand.setBackgroundResource(R.drawable.brand);
                btn_price.setBackgroundResource(R.drawable.brand_grey);
                btn_brand.setTextColor(Color.WHITE);
                btn_price.setTextColor(Color.BLACK);
                rv.setVisibility(View.VISIBLE);
                ll_holder.setVisibility(View.GONE);
                break;
            case R.id.btn_price:
                btn_brand.setBackgroundResource(R.drawable.brand_grey);
                btn_price.setBackgroundResource(R.drawable.brand);
                btn_brand.setTextColor(Color.BLACK);
                btn_price.setTextColor(Color.WHITE);
                rv.setVisibility(View.GONE);
                System.out.println(" PRICE LIST SIZE "+mApp.al_price_list.size());
                loadMenuItem();
                ll_holder.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_apply:
                String brand="";
                for (int i = 0; i < mApp.al_brand_list.size(); i++) {
                    if (mApp.al_brand_list.get(i).check == 1) {
                        System.out.println(" VALUE " + mApp.al_brand_list.get(i).value);
                        brand=brand+mApp.al_brand_list.get(i).id;
                        brand=brand+",";
                    }
                }

                if(brand.length()<1){
                    brand="";
                }else{
                    brand=brand.substring(0,brand.length()-1);
                }
                String value_price[]={"",""};
                for (int i = 0; i < mApp.al_price_list.size(); i++) {
                    if (mApp.al_price_list.get(i).check == 1) {
                        value_price= mApp.al_price_list.get(i).value.split(" - ");
                    }
                }
                System.out.println(" min max "+value_price[0].replaceAll("Rs. ","")+" "+value_price[1].replaceAll("Rs. ",""));
                mApp.pa.advanceSearchInitiate(brand,value_price[0].replaceAll("Rs. ","").trim(),value_price[1].replaceAll("Rs. ","").trim(),cat_id);
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
                break;
        }
    }

    /////////////////////// THIS BELOW SEGMENT IS ONLY FOR FRAGMENT HANDLING ????????????????????????????????????????
    public void getBrandAndPrice(String cat_id) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("catid", cat_id);

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                mApp.al_brand_list = AllUtilMethods.parseBrandList(result, AdvanceSearchFilterActivity.this);
                mApp.al_price_list = AllUtilMethods.parsePriceList(result,AdvanceSearchFilterActivity.this);
                if (((InBxApplication) AdvanceSearchFilterActivity.this.getApplication()).al_brand_list != null) {
                    setUI();
                } else {
                    finish();
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
                    finish();

            }
        }, AdvanceSearchFilterActivity.this).performPostRequest(AdvanceSearchFilterActivity.this, AllUtilUrls.base_url + "" + AllUtilUrls.cat_id, map, "Please wait ...", 0);
    }

}
