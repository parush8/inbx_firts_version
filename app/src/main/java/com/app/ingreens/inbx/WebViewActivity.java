package com.app.ingreens.inbx;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;

/**
 * Created by Papun's on 8/12/2016.
 */
public class WebViewActivity extends MyCustomActivity {

    //////////////////// UI VARIABLES ///////////////////

    private Toolbar toolbar;
    public CoordinatorLayout overview_coordinator_layout;
    private TextView toolbar_title;
    private WebView wv_web;
    private ImageView imv_cart;
    private TextView tv_cart_count;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;
    String url, page;

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(WebViewActivity.this));
        setContentView(R.layout.web_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);


        ImageView imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(WebViewActivity.this, NonHomePage.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        imv_cart = (ImageView) toolbar.findViewById(R.id.imv_cart);
        imv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(getSharedPreferences("login", 0).getString("cart_count", "0")) > 0) {
                    Intent intent = new Intent(new Intent(WebViewActivity.this, MyCartActivity.class));
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
                }
            }
        });

        tv_cart_count = (TextView) toolbar.findViewById(R.id.tv_global_cart_count);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            setUI();
        }
        resume = true;
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }

    private void setUI() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
            }
        });

        page = getIntent().getExtras().getString("page");
        if (page.equalsIgnoreCase("con")) {
            url = AllUtilUrls.contact_us;
            setToolbarTitle("Contact us");
        } else {
            url = AllUtilUrls.about_us;
            setToolbarTitle("About us");
        }

        wv_web = (WebView) findViewById(R.id.wv_web);
        wv_web.setWebViewClient(new myWebClient());
        wv_web.getSettings().setAppCacheMaxSize(5 * 1024 * 1024); // 5MB
        wv_web.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
        wv_web.getSettings().setAllowFileAccess(true);
        wv_web.getSettings().setAppCacheEnabled(true);
        wv_web.getSettings().setJavaScriptEnabled(true);
        wv_web.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        wv_web.loadUrl("" + url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        AllUtilMethods.disableMenuItemForSometimes(item);
        switch (item.getItemId()) {
            case R.id.action_home:
                intent = new Intent(new Intent(WebViewActivity.this, NonHomePage.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_search:
                intent = new Intent(new Intent(WebViewActivity.this, SearchActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            //    case R.id.action_cart:

//                intent = new Intent(new Intent(WebViewActivity.this, MyCartActivity.class));
//                startActivity(intent);
//                overridePendingTransition( R.anim.slide_up, R.anim.stay );
            //      break;
            case R.id.action_account:
                intent = new Intent(new Intent(WebViewActivity.this, MyAccount.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_contact_us:
                intent = new Intent(new Intent(WebViewActivity.this, WebViewActivity.class));
                intent.putExtra("page", "con");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_about_us:
                intent = new Intent(new Intent(WebViewActivity.this, WebViewActivity.class));
                intent.putExtra("page", "ab");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_logout:

                intent = new Intent(new Intent(WebViewActivity.this, NonLoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                getSharedPreferences("login", 0).edit().clear().commit();
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_order:

                intent = new Intent(new Intent(WebViewActivity.this, TrackYourOrderActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_change_password:
                AllUtilMethods.changePasswordDialog(WebViewActivity.this, overview_coordinator_layout);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}


