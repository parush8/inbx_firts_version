package com.app.ingreens.inbx;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.TextView;

import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;

/**
 * Created by Papun's on 8/11/2016.
 */
public class MyAccount extends MyCustomActivity implements AllUtilMethods.AccountDetailsRetrieved {
    //////////////////// UI VARIABLES ///////////////////
    private Toolbar toolbar;
    private CoordinatorLayout overview_coordinator_layout;
    private TextView toolbar_title;
    private EditText edt_name, edt_email, edt_add_1, edt_pin, edt_city, edt_state, edt_tele;
    private LinearLayout ll_bill_address;
    private ImageView imv_cart;
    private TextView tv_cart_count;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.my_account);

        overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);

        ll_bill_address = (LinearLayout) findViewById(R.id.ll_bill_address);
        ll_bill_address.setVisibility(View.GONE);

        imv_cart = (ImageView) toolbar.findViewById(R.id.imv_cart);
        imv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(getSharedPreferences("login", 0).getString("cart_count", "0")) > 0) {
                    Intent intent = new Intent(new Intent(MyAccount.this, MyCartActivity.class));
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
                } else {
                    AllUtilMethods.emptyCartDialog(MyAccount.this);
                }
            }
        });

        tv_cart_count = (TextView) toolbar.findViewById(R.id.tv_global_cart_count);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {

            AllUtilMethods.myAccount(MyAccount.this, getSharedPreferences("login", 0).getString("customer_id", ""), overview_coordinator_layout, MyAccount.this);

        }
        resume = true;
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }

    private void setUI() {

        ll_bill_address.setVisibility(View.VISIBLE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));

        ImageView imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(MyAccount.this, NonHomePage.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
            }
        });
        setToolbarTitle("My Account");


//        getSharedPreferences("login", 0).getString("log", "")
//        getSharedPreferences("login", 0).getString("email", "")
//        getSharedPreferences("login", 0).getString("customer_id", "")
//        getSharedPreferences("login", 0).getString("firstname", "")
//        getSharedPreferences("login", 0).getString("lastname", "")
//        getSharedPreferences("login", 0).getString("group_id", "")
//
//        getSharedPreferences("login", 0).getString("city","")
//        getSharedPreferences("login", 0).getString("street_address", "")
//        getSharedPreferences("login", 0).getString("postcode", "")
//        getSharedPreferences("login", 0).getString("telephone","")


        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_name.setText(getSharedPreferences("login", 0).getString("firstname", "") + " " + getSharedPreferences("login", 0).getString("lastname", ""));

        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_email.setText("" + getSharedPreferences("login", 0).getString("email", ""));

        edt_add_1 = (EditText) findViewById(R.id.edt_add_1);
        edt_add_1.setText("" + getSharedPreferences("login", 0).getString("street_address", ""));

        edt_pin = (EditText) findViewById(R.id.edt_pin);
        edt_pin.setText("" + getSharedPreferences("login", 0).getString("postcode", ""));

        edt_city = (EditText) findViewById(R.id.edt_city);
        edt_city.setText("" + getSharedPreferences("login", 0).getString("city", ""));

        edt_state = (EditText) findViewById(R.id.edt_state);
        edt_state.setEnabled(false);

        edt_tele = (EditText) findViewById(R.id.edt_tele);
        edt_tele.setText("" + getSharedPreferences("login", 0).getString("telephone", ""));

        ll_bill_address = (LinearLayout) findViewById(R.id.ll_bill_address);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        AllUtilMethods.disableMenuItemForSometimes(item);
        switch (item.getItemId()) {
            case R.id.action_home:
                intent = new Intent(new Intent(MyAccount.this, NonHomePage.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);

                break;
            case R.id.action_search:
                intent = new Intent(new Intent(MyAccount.this, SearchActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            //      case R.id.action_cart:
//                intent = new Intent(new Intent(MyAccount.this, MyCartActivity.class));
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            //        break;
            case R.id.action_account:
                intent = new Intent(new Intent(MyAccount.this, MyAccount.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                finish();
                break;
            case R.id.action_contact_us:
                intent = new Intent(new Intent(MyAccount.this, WebViewActivity.class));
                intent.putExtra("page", "con");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_about_us:
                intent = new Intent(new Intent(MyAccount.this, WebViewActivity.class));
                intent.putExtra("page", "ab");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_logout:
                intent = new Intent(new Intent(MyAccount.this, NonLoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                getSharedPreferences("login", 0).edit().clear().commit();
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_order:
                intent = new Intent(new Intent(MyAccount.this, TrackYourOrderActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_change_password:

                AllUtilMethods.changePasswordDialog(MyAccount.this, overview_coordinator_layout);

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    @Override
    public void accountDetailsRetrieved() {
        setUI();
    }

    @Override
    public void accountDetailsRetrievedError() {
        finish();
    }
}
