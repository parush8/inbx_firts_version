package com.app.ingreens.inbx;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.gcm.GcmListenerService;

import java.io.Serializable;

/**
 * Created by developer on 5/30/2016.
 */
public class PushNotificationService extends GcmListenerService {

    private NotificationManager mNotificationManager;
    PendingIntent contentIntent ;


    @Override
    public void onMessageReceived(String from, Bundle data) {

        sendNotification("ksdjfk",data.getString("message"),data.getString("status"),data.getString("orderid"));

        System.out.println("  NOTIFICATION   "+data.toString());
    }

    private void sendNotification(String title,String msg,String status,String orderid) {
        System.out.println("  NOTIFICATION   "+orderid);

           if(orderid!=null){
               contentIntent= PendingIntent.getActivity(PushNotificationService.this, 0,
                       new Intent(PushNotificationService.this, OrderDetailsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).putExtra("order_id",orderid), PendingIntent.FLAG_UPDATE_CURRENT);
           }else{
               contentIntent= PendingIntent.getActivity(PushNotificationService.this, 0,
                       new Intent(PushNotificationService.this, TrackYourOrderActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).putExtra("order_id",orderid), PendingIntent.FLAG_UPDATE_CURRENT);

           }
              mNotificationManager = (NotificationManager)
                    this.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager = (NotificationManager)
                    this.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.inbx_logo_small)
                            .setContentTitle("InBx")
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText(msg))
                            .setContentText(msg)
                            .setAutoCancel(true);
            mBuilder.setContentIntent(contentIntent);
            mBuilder.setDefaults(Notification.DEFAULT_ALL);
            long timeMillis=System.currentTimeMillis();
            timeMillis=timeMillis%1000;
            int msg_id=(int)timeMillis;
            mNotificationManager.notify(msg_id, mBuilder.build());
    }
}