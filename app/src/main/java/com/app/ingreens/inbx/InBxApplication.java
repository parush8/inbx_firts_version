package com.app.ingreens.inbx;

import android.app.Application;

import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;

import java.util.ArrayList;

/**
 * Created by root on 8/19/16.
 */
public class InBxApplication extends Application {
    public ArrayList<AllUtilSClass.Brand> al_brand_list=new ArrayList<AllUtilSClass.Brand>();
    public ArrayList<AllUtilSClass.Price> al_price_list=new ArrayList<AllUtilSClass.Price>();
    public ProductListActivity pa;
    public NonLoginActivity na;
}
