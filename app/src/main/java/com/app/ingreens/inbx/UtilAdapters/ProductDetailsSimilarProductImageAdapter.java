package com.app.ingreens.inbx.UtilAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.ingreens.inbx.ProductDetailsActivity;
import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;

import java.util.List;

/**
 * Created by Papun's on 7/17/2016.
 */
public class ProductDetailsSimilarProductImageAdapter extends RecyclerView.Adapter<ProductDetailsSimilarProductImageAdapter.MyViewHolder> {

    private List<AllUtilSClass.ProductDetailsSimilarProductImageUrl> moviesList;
    private Activity ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imv_product_details_item_image;

        public MyViewHolder(View view) {
            super(view);
            imv_product_details_item_image = (ImageView) view.findViewById(R.id.imv_product_details_item_image);

        }
    }


    public ProductDetailsSimilarProductImageAdapter(Activity ctx, List<AllUtilSClass.ProductDetailsSimilarProductImageUrl> moviesList) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_details_similar_product_image_list_cell, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        AllUtilMethods.getImage(ctx, R.drawable.subcategory_placeholder, moviesList.get(position).imageurl, holder.imv_product_details_item_image, 300, 300);

        holder.imv_product_details_item_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(ctx, ProductDetailsActivity.class));
                intent.putExtra("cat_id", moviesList.get(position).id);
                ctx.startActivity(intent);
                ctx.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
