package com.app.ingreens.inbx;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Window;

import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class SplashActivity extends MyCustomActivity {

    boolean resume;
    SharedPreferences myPref;
    SharedPreferences.Editor myPref_editor;
    String reg_id = "", msg = "";

    // //////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(SplashActivity.this));
        setContentView(R.layout.activity_splash);
        myPref = getSharedPreferences("user_data", 0);
        //System.out.println("=============HASH KEY====="+printKeyHash(SplashStart.this));
    }


    @Override
    protected void onResume() {
        if (!resume) {
            ConnectivityManager cm = (ConnectivityManager) SplashActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = false;
            try {
                isConnected = activeNetwork.isConnectedOrConnecting();
            } catch (NullPointerException n) {
                //Toast.makeText(SplashStart.this, "NET CONNECTION", Toast.LENGTH_SHORT).show();
            }

            if (isConnected) {
                if (getSharedPreferences("user_data", 0).getString("reg_id", "").length() < 1) {
                    GCMClientManager pushClientManager = new GCMClientManager(this, AllUtilUrls.SENDER_ID);
                    pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                        @Override
                        public void onSuccess(String registrationId, boolean isNewRegistration) {
                            if (isNewRegistration) {
                                Log.e("GCM Registration id", registrationId + "   " + isNewRegistration);
                                myPref_editor = myPref.edit();
                                myPref_editor.putString("reg_id", registrationId);
                                myPref_editor.commit();
                            }
                            Handler h = new Handler();
                            Runnable r = new Runnable() {
                                public void run() {
                                    setUI();
                                }
                            };
                            h.postDelayed(r, 3000);
                        }

                        @Override
                        public void onFailure(String ex) {
                            super.onFailure(ex);
                            Log.d("GCM Registration id", ex);
                            AllUtilMethods.networkDialog(" Your device can not be registered with Google to get Server messages. ", SplashActivity.this);
                        }
                    });
                } else {
                    Handler h = new Handler();
                    Runnable r = new Runnable() {
                        public void run() {
                            setUI();
                        }
                    };
                    h.postDelayed(r, 3000);
                }
            } else {
                AllUtilMethods.networkDialog("This app requires an internet connection.Make sure you are connected to a wifi network or have switched on your network data.", SplashActivity.this);
            }
        }
        resume = true;
        super.onResume();
    }

    private void setUI() {

        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    finish();
                    System.out.println("SERVICE");
                    startActivity(new Intent(SplashActivity.this, NonLoginActivity.class));
                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
                }
            });

        } catch (Exception e) {

        }

    }


    ///// this is for DEVICE KEY HASH GENERATION
//	public static String printKeyHash(Activity context) {
//		PackageInfo packageInfo;
//		String key = null;
//		try {
//			//getting application package name, as defined in manifest
//			String packageName = context.getApplicationContext().getPackageName();
//
//			//Retriving package info
//			packageInfo = context.getPackageManager().getPackageInfo(packageName,
//					PackageManager.GET_SIGNATURES);
//
//			Log.e("Package Name=", context.getApplicationContext().getPackageName());
//
//			for (android.content.pm.Signature signature : packageInfo.signatures) {
//				MessageDigest md = MessageDigest.getInstance("SHA");
//				md.update(signature.toByteArray());
//				key = new String(Base64.encode(md.digest(), 0));
//
//				// String key = new String(Base64.encodeBytes(md.digest()));
//				Log.e("Key Hash=", key);
//			}
//		} catch (NameNotFoundException e1) {
//			Log.e("Name not found", e1.toString());
//		}
//		catch (NoSuchAlgorithmException e) {
//			Log.e("No such an algorithm", e.toString());
//		} catch (Exception e) {
//			Log.e("Exception", e.toString());
//		}
//		return key;
//	}
}
