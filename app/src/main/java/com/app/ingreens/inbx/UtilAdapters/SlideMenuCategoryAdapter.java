package com.app.ingreens.inbx.UtilAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.SubCategory;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;

import java.util.List;

/**
 * Created by root on 7/8/16.
 */
public class SlideMenuCategoryAdapter extends RecyclerView.Adapter<SlideMenuCategoryAdapter.MyViewHolder> {

    private List<AllUtilSClass.Category> moviesList;
    private Activity ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_category;
        public ImageView imv_category;

        public MyViewHolder(View view) {
            super(view);
            tv_category = (TextView) view.findViewById(R.id.tv_category);
            imv_category = (ImageView) view.findViewById(R.id.imv_product_image);
        }
    }


    public SlideMenuCategoryAdapter(Activity ctx, List<AllUtilSClass.Category> moviesList) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.slide_category_list_cell, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_category.setText(moviesList.get(position).name);
        AllUtilMethods.getImage(ctx, R.drawable.subcategory_placeholder, moviesList.get(position).thumb, holder.imv_category, 100, 100);

        holder.tv_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(ctx, SubCategory.class));
                intent.putExtra("cat_id", moviesList.get(position).id);
                ctx.startActivity(intent);
                ctx.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
