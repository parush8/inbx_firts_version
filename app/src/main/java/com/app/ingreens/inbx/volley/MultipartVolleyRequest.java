package com.app.ingreens.inbx.volley;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.webkit.MimeTypeMap;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;

public class MultipartVolleyRequest extends Request<String> {

	private HashMap<String,String> paramsMap;
	private HashMap<String,File> fileParams;
	private MultipartEntity entity;
	private Listener<String> succListener;
	private Context mContext;

	public MultipartVolleyRequest(Context context,int method, String url, Listener<String> successlistener,ErrorListener listener,HashMap<String,File> fileparams,HashMap<String,String> params) {
		super(method, url, listener);
		// TODO Auto-generated constructor stub
		this.paramsMap=params;
		this.fileParams=fileparams;
		this.succListener=successlistener;
		mContext=context;
		createMultipartEntity();
	}

	@Override
	protected void deliverResponse(String arg0) {
		// TODO Auto-generated method stub
		succListener.onResponse(arg0);
	}


	private void createMultipartEntity()
	{
		MultipartEntity entity=new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

		try {
			if(paramsMap!=null)
			{
				Iterator<Entry<String,String>> entryIterator=paramsMap.entrySet().iterator();
				while(entryIterator.hasNext())
				{
					Entry<String,String> item=entryIterator.next();
					entity.addPart(item.getKey(), new StringBody(item.getValue()));
				}
			}

			if(fileParams!=null)
			{
				Iterator<Entry<String,File>> fileIterator=fileParams.entrySet().iterator();
				while(fileIterator.hasNext())
				{
					Entry<String,File> item=fileIterator.next();
					if(getMimeTypeOfFile(Uri.fromFile(item.getValue())).contains("video"))
					{
						entity.addPart(item.getKey(),new FileBody(item.getValue()));

					}
					else if(getMimeTypeOfFile(Uri.fromFile(item.getValue())).contains("audio"))
					{
						entity.addPart(item.getKey(), new FileBody(item.getValue()));

					}
					else if(getMimeTypeOfFile(Uri.fromFile(item.getValue())).contains("image"))
					{
						entity.addPart(item.getKey(), getImageContentBody(item.getValue().getPath()));
					}
				}
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.entity=entity;
	}



	@SuppressWarnings("deprecation")
	@Override
	public String getBodyContentType() {
		// TODO Auto-generated method stub
		return entity.getContentType().getValue();
	}

	@Override
	protected Response<String> parseNetworkResponse(NetworkResponse arg0) {
		// TODO Auto-generated method stub
		try {
			return Response.success(new String(arg0.data, "UTF-8"),
			        getCacheEntry());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}



	@Override
	public byte[] getBody() throws AuthFailureError {
		// TODO Auto-generated method stub
		ByteArrayOutputStream bos=new ByteArrayOutputStream();
		try {
			entity.writeTo(bos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bos.toByteArray();
	}



	private ContentBody getImageContentBody(String filepath) {
		ContentBody enc = null;
		if (filepath.length() > 0) {
			System.out.println("Path of the file is : "+filepath);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			Bitmap bitmap;
			try {
				bitmap = BitmapFactory.decodeFile(filepath, options);
			} catch (Exception e) {
				// p_diag.dismiss();
				return null;
			}

			if (bitmap.getHeight() > 800 && bitmap.getWidth() > 600) {
				Bitmap b;			
				if(bitmap.getHeight()<bitmap.getWidth())
				{
					b = getResizedBitmap(bitmap, 610, 950);
				}
				else
				{
					b = getResizedBitmap(bitmap,950, 610);
				}				File cacheDir = ((Activity) mContext).getBaseContext().getCacheDir();
				File f = new File(cacheDir, "temp_"+System.currentTimeMillis()+".jpg");
				try {
					FileOutputStream out = new FileOutputStream(f);
					b.compress(Bitmap.CompressFormat.JPEG, 85, out);
					out.flush();
					out.close();
					enc = new FileBody(f);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return null;
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
			} else {
				File file = new File(filepath);
				enc = new FileBody(file);
			}
		} else {
			try {
				enc = new StringBody("");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return enc;
	}



	public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
		int width = bm.getWidth();
		int height = bm.getHeight();
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		// CREATE A MATRIX FOR THE MANIPULATION
		Matrix matrix = new Matrix();
		// RESIZE THE BIT MAP
		matrix.postScale(scaleWidth, scaleHeight);
//		matrix.postRotate(KicksList_Activity.rotation);
		// "RECREATE" THE NEW BITMAP
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
				matrix, false);
		return resizedBitmap;
	}

	private String getMimeTypeOfFile(Uri uri)
	{

		String fileExtension
		= MimeTypeMap.getFileExtensionFromUrl(uri.toString());
		String mimeType
		= MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
		return mimeType;
	}
}
