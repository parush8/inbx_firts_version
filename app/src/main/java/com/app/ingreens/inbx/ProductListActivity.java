package com.app.ingreens.inbx;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilAdapters.ProductListAdpter;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Papun's on 7/23/2016.
 */
public class ProductListActivity extends MyCustomActivity {
    //////////////////// UI VARIABLES ///////////////////
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    FrameLayout fm_left_slide_menu, fm_container;
    private CoordinatorLayout overview_coordinator_layout;
    private TextView toolbar_title;
    private RecyclerView rv;
    private ImageView imv_cart;
    private TextView tv_cart_count;
    private ImageButton fav_filter;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume, state;//STATE IS USED FOR LOAD MORE
    private String cat_id = "", brand = "", min_price = "", max_price = "";
    private int page = 1, advance_search_page = 1, advance_search_flag = 0;
    private String next = "false";
    private ArrayList<AllUtilSClass.ProductList> al_product_list;

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(ProductListActivity.this));
        setContentView(R.layout.project_list);

        overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        cat_id = getIntent().getStringExtra("cat_id");

        ImageView imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(ProductListActivity.this, NonHomePage.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        imv_cart = (ImageView) toolbar.findViewById(R.id.imv_cart);
        imv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(getSharedPreferences("login", 0).getString("cart_count", "0")) > 0) {
                    Intent intent = new Intent(new Intent(ProductListActivity.this, MyCartActivity.class));
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
                }
            }
        });

        tv_cart_count = (TextView) toolbar.findViewById(R.id.tv_global_cart_count);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            final InBxApplication mApp = ((InBxApplication) ProductListActivity.this.getApplicationContext());
            mApp.pa = this;
            getProductList(cat_id, 0);
        }
        resume = true;
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }

    private void setUI() {

        System.out.println("   LOGGED IN DETAILS   " + getSharedPreferences("login", 0).getString("email", "")
                + "  " + getSharedPreferences("login", 0).getString("customer_id", "")
                + " " + getSharedPreferences("login", 0).getString("firstname", "")
                + "  " + getSharedPreferences("login", 0).getString("lastname", ""));

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
            }
        });
        setToolbarTitle("Product List");
        System.out.println(" CATID " + cat_id);
        fav_filter = (ImageButton) findViewById(R.id.fav_filter);
        fav_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cat_id.equalsIgnoreCase(getSharedPreferences("login", 0).getString("brand_cat_id", ""))) {

                } else {
                    ((InBxApplication) ProductListActivity.this.getApplication()).al_brand_list = new ArrayList<AllUtilSClass.Brand>();
                    getSharedPreferences("login", 0).edit().putString("brand_cat_id", cat_id).commit();
                }
                Intent intent = new Intent(new Intent(ProductListActivity.this, AdvanceSearchFilterActivity.class));
                intent.putExtra("cat_id", cat_id);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        final ProductListAdpter ama = new ProductListAdpter(rv, ProductListActivity.this, al_product_list, overview_coordinator_layout);
        rv = (RecyclerView) findViewById(R.id.rv);
        final GridLayoutManager lLayout = new GridLayoutManager(ProductListActivity.this, 2);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(lLayout);
        rv.setAdapter(ama);
        AllUtilMethods.setRVAnimation(rv);

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    if (next.equalsIgnoreCase("true")) {
                        if (AllUtilMethods.isLastItemDisplaying(rv)) {
                            Handler handler = new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    super.handleMessage(msg);
                                    if (advance_search_flag == 1) {
                                        advance_search_page++;
                                        advanceSearchBrandAndPrice(brand, min_price, max_price, advance_search_page, 1, cat_id);
                                    } else {
                                        page++;
                                        System.out.println(" END ");
                                        getProductList(cat_id, 1);
                                    }
                                }
                            };
                            handler.sendEmptyMessageDelayed(0, 1000);
                        }
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        AllUtilMethods.disableMenuItemForSometimes(item);
        switch (item.getItemId()) {
            case R.id.action_home:
                intent = new Intent(new Intent(ProductListActivity.this, NonHomePage.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);

                break;
            case R.id.action_search:
                intent = new Intent(new Intent(ProductListActivity.this, SearchActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            //         case R.id.action_cart:
//                intent = new Intent(new Intent(ProductListActivity.this, MyCartActivity.class));
//                startActivity(intent);
//                overridePendingTransition( R.anim.slide_up, R.anim.stay );
            //           break;
            case R.id.action_account:
                intent = new Intent(new Intent(ProductListActivity.this, MyAccount.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_contact_us:
                intent = new Intent(new Intent(ProductListActivity.this, WebViewActivity.class));
                intent.putExtra("page", "con");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_about_us:
                intent = new Intent(new Intent(ProductListActivity.this, WebViewActivity.class));
                intent.putExtra("page", "ab");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_logout:
                intent = new Intent(new Intent(ProductListActivity.this, NonLoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                getSharedPreferences("login", 0).edit().clear().commit();
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_order:
                intent = new Intent(new Intent(ProductListActivity.this, TrackYourOrderActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_change_password:
                AllUtilMethods.changePasswordDialog(ProductListActivity.this, overview_coordinator_layout);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }


    public void getProductList(String cat_id, final int flag) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", getSharedPreferences("login", 0).getString("customer_id", "0"));
        map.put("page", "" + page);
        String msg = "";
        if (flag == 1) {
            msg = "Loading More ...";
        }
        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    System.out.println(" JSON jskdjfkasf " + result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        next = pdata.getString("lastpage");
                        al_product_list = AllUtilMethods.parseProductList(result, flag, al_product_list);
                        if (al_product_list.size() > 0) {
                            if (page == 1) {
                                setUI();
                            } else {
                                rv.getAdapter().notifyDataSetChanged();
                            }

                        } else {
                        }
                    } else {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "No Product list found.");
                        finish();
                    }
                } catch (JSONException e) {
                    System.out.println(" JSON EXCEPTION " + e);
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                    finish();

                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
                finish();

            }
        }, ProductListActivity.this).performPostRequest(ProductListActivity.this, AllUtilUrls.base_url + "" + AllUtilUrls.product_lis + cat_id, map, msg, 0);
    }

    /////////////////////// THIS BELOW SEGMENT IS ONLY FOR FRAGMENT HANDLING ????????????????????????????????????????


    @Override
    public void onCartItemUpdate() {
        super.onCartItemUpdate();
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }


    /////////////////////////////////////////////////////////////////////////////
    public void advanceSearchInitiate(String brands, String min_price, String max_price, String cat_id) {
        advance_search_flag = 1;
        advance_search_page = 1;
        this.brand = brands;
        this.min_price = min_price;
        this.max_price = max_price;
        this.cat_id = cat_id;
        advanceSearchBrandAndPrice(brand, min_price, max_price, advance_search_page, 0, cat_id);
    }

    public void advanceSearchBrandAndPrice(String brands, String min_price, String max_price, final int page, final int flag, String cat_id) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("price_from", min_price);
        map.put("customer_id", getSharedPreferences("login", 0).getString("customer_id", "0"));
        map.put("price_to", max_price);
        map.put("brands", brands);
        map.put("page", "" + page);
        map.put("catid", cat_id);
        String msg = "";
        if (flag == 1) {
            msg = "Loading More ...";
        }

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    System.out.println(" JSON jskdjfkasf " + result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        next = pdata.getString("lastpage");
                        al_product_list = AllUtilMethods.parseProductList(result, flag, al_product_list);
                        if (al_product_list.size() > 0) {
                            if (advance_search_page == 1) {
                                setUI();
                            } else {
                                rv.getAdapter().notifyDataSetChanged();
                            }

                        } else {
                            AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "No search item has found..");
                        }
                    } else {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "No Product list found.");
                        finish();

                    }
                } catch (JSONException e) {
                    System.out.println(" JSON EXCEPTION " + e);
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                    finish();
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
                finish();
            }
        }, ProductListActivity.this).performPostRequest(ProductListActivity.this, AllUtilUrls.base_url + "" + AllUtilUrls.advance_search, map, msg, 0);
    }

}
