package com.app.ingreens.inbx.volley;//package com.ncrts.volley;
//
//import java.util.HashMap;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.Context;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.os.Handler;
//import android.text.Html;
//import android.text.Spanned;
//import android.view.View;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.view.animation.Interpolator;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.Toast;
//import android.widget.LinearLayout.LayoutParams;
//import android.widget.TextView;
//
//import com.ncrts.utils.AllUrls;
//import com.ncrts.waytless.R;
//
//public class CustomDialog extends Dialog implements android.view.View.OnClickListener{
//
//	private ImageView imageFlip,imageCricle;
//	private Button btn_cancel_order;
//	private Handler	AnimHandler1 = new Handler();
//	private final int Time = 500;
//	private final int Time2 = 2000;
//	private Context mContext;
//	TextView tv_alert_message;
//	String text_msg;
//	Spanned sp_msg;
//	int which_constructor=0,cancel=0;
//	
//	public CustomDialog(Context context,String msg,int cancel) {     
//		super(context);
//		setCancelable(false);
//		text_msg=msg;
//		which_constructor=0;
//		this.cancel=cancel;
//		mContext = context;
//		getWindow().setBackgroundDrawable(new ColorDrawable(android.R.drawable.screen_background_dark_transparent));
//		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//
//	}
//
//	public CustomDialog(Context context,Spanned msg,int cancel) {     
//		super(context);
//		setCancelable(false);
//		sp_msg=msg;
//		this.cancel=cancel;
//		which_constructor=1;
//		mContext = context;
//		getWindow().setBackgroundDrawable(new ColorDrawable(android.R.drawable.screen_background_dark_transparent));
//		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//
//	}
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.dialog);
//		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//
//		if(which_constructor==0){
//			initialize(text_msg);
//		}else{
//			initialize(sp_msg);
//		}
//		
//		Animation a = AnimationUtils.loadAnimation((Activity)mContext, R.anim.minute_rotate_indefinitely);
//		a.setDuration(Time2);
//		imageCricle.startAnimation(a);
//
//		a.setInterpolator(new Interpolator()
//		{
//			private final int frameCount = 50;
//
//			@Override
//			public float getInterpolation(float input)
//			{
//				return (float)Math.floor(input*frameCount)/frameCount;
//			}
//		});
//		AnimHandler1.post(new Runnable1());
//	}
//
//	private void initialize(String msg) {
//
//		imageCricle = (ImageView) findViewById(R.id.image_circle);
//		imageFlip 	= (ImageView) findViewById(R.id.image_flipper);
//		
//		btn_cancel_order= (Button) findViewById(R.id.btn_cancel_order);
//		if(cancel==0){
//			btn_cancel_order.setVisibility(View.GONE);
//		}else{
//			btn_cancel_order.setVisibility(View.VISIBLE);
//		}
//		btn_cancel_order.setOnClickListener(this);
//		tv_alert_message=(TextView)findViewById(R.id.tv_alert_message);
//		tv_alert_message.setText(""+msg);
//	}
//
//	private void initialize(Spanned msg) {
//
//		imageCricle = (ImageView) findViewById(R.id.image_circle);
//		imageFlip 	= (ImageView) findViewById(R.id.image_flipper);
//
//		btn_cancel_order= (Button) findViewById(R.id.btn_cancel_order);
//		if(cancel==0){
//			btn_cancel_order.setVisibility(View.GONE);
//		}else{
//			btn_cancel_order.setVisibility(View.VISIBLE);
//		}
//		
//		btn_cancel_order.setOnClickListener(this);
//		
//		tv_alert_message=(TextView)findViewById(R.id.tv_alert_message);
//		tv_alert_message.setText(Html.fromHtml("<b>Please wait</b> while we check the<br>restaurant's availability..."));
//	}
//
//	private class Runnable1 implements Runnable {
//
//		@Override
//		public void run() {
//			Anime1();
//			AnimHandler1.postDelayed(new Runnable2(),Time);
//		}
//	}
//
//	private class Runnable2 implements Runnable {
//
//		@Override
//		public void run() {
//			Anime2();
//			AnimHandler1.postDelayed(new Runnable1(),Time);
//		}
//	}
//
//	private void Anime1() {
//
//		Animation b = AnimationUtils.loadAnimation((Activity)mContext, R.anim.rotate_indefinitely);
//		b.setDuration(Time);
//		imageFlip.startAnimation(b);
//
//		b.setInterpolator(new Interpolator()
//		{
//			private final int frameCount = 50;
//
//			@Override
//			public float getInterpolation(float input)
//			{
//				return (float)Math.floor(input*frameCount)/frameCount;
//			}
//		});
//
//	}
//	private void Anime2() {
//
//		Animation c = AnimationUtils.loadAnimation((Activity)mContext, R.anim.rotate_indefinitely);
//		c.setDuration(Time);
//		imageFlip.startAnimation(c);
//
//		c.setInterpolator(new Interpolator()
//		{
//			private final int frameCount = 50;
//
//			@Override
//			public float getInterpolation(float input)
//			{
//				return (float)Math.floor(input*frameCount)/frameCount;
//			}
//		});
//	}
//
//	@Override
//	public void onClick(View v) {
//		switch(v.getId()){
//		case R.id.btn_cancel_order:
//			System.out.println(" cancel working ");
//			this.dismiss();
//			GlobalVariable.getInstance(mContext).cancelPendingRequests(GlobalVariable.TAG);
//			cancelBooking();
//			break;
//		}
//		
//	}
//	private void cancelBooking() {
//
//
//		HashMap<String,String> map=new HashMap<String, String>();
//
//		map.put("loggedin_user", mContext.getSharedPreferences("login_data", 0).getString("user_no", ""));
//		map.put("booking_id", mContext.getSharedPreferences("booking_details", 0).getString("booking_id", ""));
//	
//
//		new GeneralNetworkRequestClass(new CommonCallback() {
//			@Override
//			public void onNetworkProcessingCompleted(String result) {
//				try {
//					System.out.println( " JSON RESULT "+result);
//					JSONObject status = new JSONObject(result);
//
//					if(status.getString("result").equalsIgnoreCase("true")){
//						
//					}else{
//						
//					}
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//			}
//			@Override
//			public void onErrorEncountered(String msg) {
//				makeToast(msg);
//			}
//		},mContext).performPostRequest(mContext, AllUrls.base_url+""+AllUrls.cancel_booking,map,"Please wait ...",0);	
//	
//		}
//	
//	public void makeToast(String msg)
//	{
//		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
//	}
//	}
