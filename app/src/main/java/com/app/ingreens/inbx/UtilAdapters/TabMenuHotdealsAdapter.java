package com.app.ingreens.inbx.UtilAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.ProductDetailsActivity;
import com.app.ingreens.inbx.ProductListActivity;
import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;

import java.util.List;

/**
 * Created by root on 7/11/16.
 */
public class TabMenuHotdealsAdapter extends RecyclerView.Adapter<TabMenuHotdealsAdapter.MyViewHolder> {

    private List<AllUtilSClass.HotDeals> moviesList;
    private Activity ctx;
    private CoordinatorLayout overview_coordinator_layout;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_price, tv_rgprice, tv_tier_prices,tv_percentage;
        public ImageView imv_category;
        public Button btn_add_to_cart;

        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_price = (TextView) view.findViewById(R.id.tv_price);
            tv_rgprice = (TextView) view.findViewById(R.id.tv_rgprice);
            imv_category = (ImageView) view.findViewById(R.id.imv_product_image);
            tv_tier_prices = (TextView) view.findViewById(R.id.tv_tier_prices);
            btn_add_to_cart = (Button) view.findViewById(R.id.btn_add_to_cart);
            tv_percentage= (TextView) view.findViewById(R.id.tv_percentage);
        }
    }


    public TabMenuHotdealsAdapter(Activity ctx, List<AllUtilSClass.HotDeals> moviesList, CoordinatorLayout overview_coordinator_layout) {
        this.overview_coordinator_layout = overview_coordinator_layout;
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_cell, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_title.setText(moviesList.get(position).name);
        holder.tv_title.setSelected(true);

        if (moviesList.get(position).stock_status.equalsIgnoreCase("0")) {
            holder.btn_add_to_cart.setText("Out of Stock");
            holder.btn_add_to_cart.setEnabled(false);
        } else {
            holder.btn_add_to_cart.setText("Add to Cart");
            holder.btn_add_to_cart.setEnabled(true);
        }

        if (moviesList.get(position).spprice.length() > 3) {
            System.out.println(" sd "+position);
            holder.tv_price.setText("" + moviesList.get(position).spprice);
            holder.tv_rgprice.setVisibility(View.VISIBLE);
            holder.tv_rgprice.setText("M.R.P. " + moviesList.get(position).rgprice);
            holder.tv_percentage.setVisibility(View.VISIBLE);
            holder.tv_rgprice.setPaintFlags(holder.tv_rgprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            String sale_price=moviesList.get(position).spprice;
            sale_price=sale_price.replace("Rs. ","").replace(",","").trim();

            String mrp_price=moviesList.get(position).rgprice;
            mrp_price=mrp_price.replace("Rs. ","").replace(",","").trim();

            holder.tv_percentage.setText(""+AllUtilMethods.getPercentage(Double.parseDouble(sale_price),Double.parseDouble(mrp_price))+"% Off.");
        } else {
            System.out.println(" ds ");
            holder.tv_price.setText("M.R.P. " + moviesList.get(position).rgprice);
            holder.tv_rgprice.setVisibility(View.INVISIBLE);
            holder.tv_percentage.setVisibility(View.INVISIBLE);
        }
        AllUtilMethods.getImage(ctx, R.drawable.subcategory_placeholder, moviesList.get(position).image_url, holder.imv_category, 300, 300);


        holder.imv_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(ctx, ProductDetailsActivity.class));
                intent.putExtra("cat_id", moviesList.get(position).id);
                ctx.startActivity(intent);
                ctx.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });
        if (moviesList.get(position).stock_status.equalsIgnoreCase("1")) {
            holder.btn_add_to_cart.setVisibility(View.VISIBLE);
        } else {
            holder.btn_add_to_cart.setVisibility(View.INVISIBLE);
        }

        holder.btn_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AllUtilMethods.addToCart(ctx,
                        ctx.getSharedPreferences("login", 0).getString("shoppingcartid", "")
                        , ctx.getSharedPreferences("login", 0).getString("customer_id", "")
                        , moviesList.get(position).id, "1",
                        overview_coordinator_layout, 0);
            }
        });
        if (moviesList.get(position).tierPrices.length() > 2) {
            holder.tv_tier_prices.setText(""+moviesList.get(position).tierPrices);
            holder.tv_tier_prices.setVisibility(View.VISIBLE);
        } else {
            holder.tv_tier_prices.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
