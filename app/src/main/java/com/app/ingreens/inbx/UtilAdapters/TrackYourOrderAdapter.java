package com.app.ingreens.inbx.UtilAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ingreens.inbx.NonHomePage;
import com.app.ingreens.inbx.OrderDetailsActivity;
import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 7/18/16.
 */
public class TrackYourOrderAdapter extends RecyclerView.Adapter<TrackYourOrderAdapter.MyViewHolder> {

    private List<AllUtilSClass.TrackOrder> moviesList;
    Activity ctx;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_order_date, tv_quantity, tv_product_code, tv_price;//, tv_order_placed_date, tv_order_shipped_date, tv_warhouse_date, tv_Delivery_date;
        public CheckBox chb_placed, chk_shipped, chk_delivered, chb_cancelled;
        public Button btn_details;
        public LinearLayout ll_holder;
        public MyViewHolder(View view) {
            super(view);

            tv_quantity = (TextView) view.findViewById(R.id.tv_quantity);
            tv_product_code = (TextView) view.findViewById(R.id.tv_product_code);
            tv_price = (TextView) view.findViewById(R.id.tv_price);
            btn_details= (Button) view.findViewById(R.id.btn_details);
           // tv_order_placed_date = (TextView) view.findViewById(R.id.tv_order_placed_date);
            //tv_order_shipped_date = (TextView) view.findViewById(R.id.tv_order_shipped_date);
           // tv_warhouse_date = (TextView) view.findViewById(R.id.tv_warhouse_date);
           // tv_Delivery_date = (TextView) view.findViewById(R.id.tv_Delivery_date);
            tv_order_date = (TextView) view.findViewById(R.id.tv_order_date);

            chb_placed = (CheckBox) view.findViewById(R.id.chb_placed);
            chk_shipped = (CheckBox) view.findViewById(R.id.chk_shipped);
            chk_delivered = (CheckBox) view.findViewById(R.id.chk_delivery);
            chb_cancelled = (CheckBox) view.findViewById(R.id.chb_cancel);

            ll_holder=(LinearLayout)view.findViewById(R.id.ll_holder);
        }
    }

    public TrackYourOrderAdapter(Activity ctx, List<AllUtilSClass.TrackOrder> moviesList) {
        this.moviesList = moviesList;
        this.ctx=ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.track_your_order_list_cell, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date curDate = new Date(moviesList.get(position).orderDate);
        String DateToStr = format.format(curDate);
        format = new SimpleDateFormat("E, dd MMM yyyy");
        DateToStr = format.format(curDate);

        holder.tv_order_date.setText(""+DateToStr);
        holder.tv_product_code.setText("Order ID: "+moviesList.get(position).orderId);
        holder.tv_quantity.setText("");
        holder.tv_price.setText(""+moviesList.get(position).orderTotal);

        holder.btn_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(ctx, OrderDetailsActivity.class));
                intent.putExtra("order_id",moviesList.get(position).id);
                ctx.startActivity(intent);
                ctx.overridePendingTransition( R.anim.slide_up, R.anim.stay );
            }
        });

        if(moviesList.get(position).orderStatus.equalsIgnoreCase("Pending")){
            holder.chk_delivered.setChecked(false);
            holder.chb_placed.setChecked(true);
            holder.chk_shipped.setChecked(false);
            holder.chb_cancelled.setChecked(false);
        }else if(moviesList.get(position).orderStatus.equalsIgnoreCase("Processing")){
            holder.chk_delivered.setChecked(false);
            holder.chb_placed.setChecked(true);
            holder.chk_shipped.setChecked(true);
            holder.chb_cancelled.setChecked(false);
        }
        else if(moviesList.get(position).orderStatus.equalsIgnoreCase("Complete")){
            holder.chk_delivered.setChecked(true);
            holder.chb_placed.setChecked(true);
            holder.chk_shipped.setChecked(true);
            holder.chb_cancelled.setChecked(false);
        }
        else if(moviesList.get(position).orderStatus.equalsIgnoreCase("Canceled") || moviesList.get(position).orderStatus.equalsIgnoreCase("Closed")){
            holder.chk_delivered.setChecked(false);
            holder.chb_placed.setChecked(false);
            holder.chk_shipped.setChecked(false);
            holder.chb_cancelled.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public interface CategoryListItemListener {
        public void onItemClick(int position, View v);
    }
}
