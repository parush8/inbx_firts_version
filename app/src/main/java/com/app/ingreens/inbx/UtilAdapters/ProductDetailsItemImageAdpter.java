package com.app.ingreens.inbx.UtilAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.ProductDetailsActivity;
import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;

import java.util.List;

/**
 * Created by Papun's on 7/17/2016.
 */
public class ProductDetailsItemImageAdpter extends RecyclerView.Adapter<ProductDetailsItemImageAdpter.MyViewHolder> {

    private List<AllUtilSClass.ProductDetailsImageUrl> moviesList;
    private Activity ctx;
    private OnImageClickCallback mlistenr;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imv_product_details_item_image;

        public MyViewHolder(View view) {
            super(view);
            imv_product_details_item_image = (ImageView) view.findViewById(R.id.imv_product_details_item_image);

        }
    }


    public ProductDetailsItemImageAdpter(Activity ctx, List<AllUtilSClass.ProductDetailsImageUrl> moviesList, OnImageClickCallback mlistenr) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.mlistenr = mlistenr;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_details_item_list_cell, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        AllUtilMethods.getImage(ctx, R.drawable.subcategory_placeholder, moviesList.get(position).image, holder.imv_product_details_item_image, 300, 300);

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public interface OnImageClickCallback {
        public void onImageClick(String url);
    }

}
