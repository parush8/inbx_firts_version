package com.app.ingreens.inbx;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Papun's on 7/23/2016.
 */
public class MyCartActivity extends MyCustomActivity implements AllUtilMethods.AddCouponUpdate, View.OnClickListener, AllUtilMethods.MyCartGetDataCallback {

    //////////////////// UI VARIABLES ///////////////////
    private Toolbar toolbar;
    private CoordinatorLayout overview_coordinator_layout;
    private TextView toolbar_title, tv_item_count, tv_price, tv_payable_amount, tv_delivery_charge;
    private LinearLayout ll_holder;
    private EditText edt_coupon_code;
    private Button btn_proceed_to_check_out, btn_ok;
    private ImageView imv_cart;
    private TextView tv_cart_count;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;
    AllUtilSClass.MyCartDetails my_cart_details = null;
    private ArrayList<AllUtilSClass.CheckOutViewHolder> al_viewholder;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(MyCartActivity.this));
        setContentView(R.layout.my_cart);

        overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        ImageView imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(MyCartActivity.this, NonHomePage.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        imv_cart = (ImageView) toolbar.findViewById(R.id.imv_cart);
        imv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(getSharedPreferences("login", 0).getString("cart_count", "0")) > 0) {
                    Intent intent = new Intent(new Intent(MyCartActivity.this, MyCartActivity.class));
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
                } else {
                    AllUtilMethods.emptyCartDialog(MyCartActivity.this);
                }
            }
        });
        tv_cart_count = (TextView) toolbar.findViewById(R.id.tv_global_cart_count);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {

            callMyCartList();
        }
        resume = true;
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }

    private void setUI() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));

        tv_item_count = (TextView) findViewById(R.id.tv_item_count);
        tv_price = (TextView) findViewById(R.id.tv_item_price);
        tv_payable_amount = (TextView) findViewById(R.id.tv_payable_amount);
        tv_delivery_charge = (TextView) findViewById(R.id.tv_delivery_charge);

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
            }
        });
        setToolbarTitle("My Cart");

        btn_proceed_to_check_out = (Button) findViewById(R.id.btn_proceed_to_check_out);
        btn_proceed_to_check_out.setOnClickListener(this);

        tv_item_count.setText("Total Price(" + my_cart_details.totalQty + " Products)");
        tv_price.setText("" + my_cart_details.subtotal);
        tv_payable_amount.setText("" + my_cart_details.grandTotal);

        if(my_cart_details.shipping_info!=null || !my_cart_details.shipping_info.isEmpty()){
            tv_delivery_charge.setText("" + my_cart_details.shipping_info);
        }

        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(this);

        edt_coupon_code = (EditText) findViewById(R.id.edt_coupon_code);

        ll_holder = (LinearLayout) findViewById(R.id.ll_holder);
        loadMenuItem();

    }

    private void loadMenuItem() {
        ll_holder.removeAllViews();
        al_viewholder = new ArrayList<AllUtilSClass.CheckOutViewHolder>();
        for (int i = 0; i < my_cart_details.al_cart_items.size(); i++) {
            ll_holder.addView(addMenuView(i));
        }
        System.out.println(" TOTAL ");
    }


    public View addMenuView(final int position) {
        View v;
        v = LayoutInflater.from(MyCartActivity.this).inflate(R.layout.my_cart_list_cell, null);
        AllUtilSClass a = new AllUtilSClass();
        AllUtilSClass.CheckOutViewHolder cvh = a.new CheckOutViewHolder();

        cvh.imv_product_image = (ImageView) v.findViewById(R.id.imv_product_image);

        AllUtilMethods.getImage(MyCartActivity.this
                , R.drawable.subcategory_placeholder
                , my_cart_details.al_cart_items.get(position).imageurl
                , cvh.imv_product_image, 200, 200);


        cvh.tv_count = (TextView) v.findViewById(R.id.tv_count);

        try {
            cvh.tv_count.setText(my_cart_details.al_cart_items.get(position).qty);
        } catch (Exception e) {
            cvh.tv_count.setText("0");
        }

        cvh.count = Integer.parseInt(my_cart_details.al_cart_items.get(position).qty);

        cvh.tv_title = (TextView) v.findViewById(R.id.tv_title);
        cvh.tv_title.setText(my_cart_details.al_cart_items.get(position).name);
        cvh.tv_title.setSelected(true);

        cvh.tv_price = (TextView) v.findViewById(R.id.tv_price);
        cvh.tv_price.setText(my_cart_details.al_cart_items.get(position).price);

        cvh.btn_minus = (ImageButton) v.findViewById(R.id.btn_minus);
        cvh.btn_minus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(my_cart_details.al_cart_items.get(position).qty) - 1;
                if (al_viewholder.get(position).count <= 1) {
                    new AlertDialog.Builder(MyCartActivity.this)
                            .setTitle("InBx")
                            .setMessage("Are you sure you want to delete this entry?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    int res = AllUtilMethods.removeCartItem(MyCartActivity.this,
                                            getSharedPreferences("login", 0).getString("shoppingcartid", "")
                                            , getSharedPreferences("login", 0).getString("customer_id", "")
                                            , my_cart_details.al_cart_items.get(position).productid,
                                            overview_coordinator_layout, MyCartActivity.this);
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                } else {
                    int res = AllUtilMethods.updateCart(MyCartActivity.this,
                            getSharedPreferences("login", 0).getString("shoppingcartid", "")
                            , getSharedPreferences("login", 0).getString("customer_id", "")
                            , my_cart_details.al_cart_items.get(position).productid, "" + qty,
                            overview_coordinator_layout, MyCartActivity.this);
                }
            }
        });
        cvh.btn_plus = (ImageButton) v.findViewById(R.id.btn_plus);
        cvh.btn_plus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(my_cart_details.al_cart_items.get(position).qty) + 1;
                int res = AllUtilMethods.updateCart(MyCartActivity.this,
                        getSharedPreferences("login", 0).getString("shoppingcartid", "")
                        , getSharedPreferences("login", 0).getString("customer_id", "")
                        , my_cart_details.al_cart_items.get(position).productid, "" + qty,
                        overview_coordinator_layout, MyCartActivity.this);

            }
        });

        cvh.btn_trash = (ImageButton) v.findViewById(R.id.btn_trash);
        cvh.btn_trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MyCartActivity.this)
                        .setTitle("InBx")
                        .setMessage("Are you sure you want to delete this entry?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                int res = AllUtilMethods.removeCartItem(MyCartActivity.this,
                                        getSharedPreferences("login", 0).getString("shoppingcartid", "")
                                        , getSharedPreferences("login", 0).getString("customer_id", "")
                                        , my_cart_details.al_cart_items.get(position).productid,
                                        overview_coordinator_layout, MyCartActivity.this);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        al_viewholder.add(cvh);
        return v;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        AllUtilMethods.disableMenuItemForSometimes(item);
        switch (item.getItemId()) {
            case R.id.action_home:
                intent = new Intent(new Intent(MyCartActivity.this, NonHomePage.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);

                break;
            case R.id.action_search:
                intent = new Intent(new Intent(MyCartActivity.this, SearchActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
//            case R.id.action_cart:
//                intent = new Intent(new Intent(MyCartActivity.this, MyCartActivity.class));
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_up, R.anim.stay);
//                break;
            case R.id.action_account:
                intent = new Intent(new Intent(MyCartActivity.this, MyAccount.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_contact_us:
                intent = new Intent(new Intent(MyCartActivity.this, WebViewActivity.class));
                intent.putExtra("page", "con");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_about_us:
                intent = new Intent(new Intent(MyCartActivity.this, WebViewActivity.class));
                intent.putExtra("page", "ab");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_logout:
                intent = new Intent(new Intent(MyCartActivity.this, NonLoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                getSharedPreferences("login", 0).edit().clear().commit();
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_order:
                intent = new Intent(new Intent(MyCartActivity.this, TrackYourOrderActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_change_password:
                AllUtilMethods.changePasswordDialog(MyCartActivity.this, overview_coordinator_layout);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_proceed_to_check_out:
                Intent intent = new Intent(new Intent(MyCartActivity.this, ChangeAddess.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.btn_ok:
                if (edt_coupon_code.getText().toString().trim().length() > 2) {
                    int res = AllUtilMethods.addCouponCode(MyCartActivity.this, getSharedPreferences("login", 0).getString("shoppingcartid", ""), edt_coupon_code.getText().toString().trim(), overview_coordinator_layout);
                    if (res == 1) {
                        callMyCartList();
                    }
                } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Please enter coupon code");

                }
                AllUtilMethods.hideKeyboard(MyCartActivity.this);
                break;
        }
    }

    public void getCartItemList(String cust_id, String shoppingcartid) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", cust_id);
        map.put("shoppingcartid", shoppingcartid);

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                my_cart_details = AllUtilMethods.parseCartList(result, MyCartActivity.this);
                if (my_cart_details != null) {
                    setUI();
                } else {
                    finish();
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
                finish();
            }
        }, MyCartActivity.this).performPostRequest(MyCartActivity.this, AllUtilUrls.base_url + "" + AllUtilUrls.get_cart_list, map, "Please wait ...", 0);
    }

    @Override
    public void callMyCartList() {
        getCartItemList(getSharedPreferences("login", 0).getString("customer_id", "0"),
                getSharedPreferences("login", 0).getString("shoppingcartid", ""));
    }

    @Override
    public void removeMyCartList() {
        getCartItemList(getSharedPreferences("login", 0).getString("customer_id", "0"),
                getSharedPreferences("login", 0).getString("shoppingcartid", ""));
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "MyCart Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.app.ingreens.inbx/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "MyCart Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.app.ingreens.inbx/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public void addCouponUpdate() {
        callMyCartList();
    }

    @Override
    public void onCartItemUpdate() {
        super.onCartItemUpdate();
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }
}
