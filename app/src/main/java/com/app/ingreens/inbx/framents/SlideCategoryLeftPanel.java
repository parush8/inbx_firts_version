package com.app.ingreens.inbx.framents;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.UtilAdapters.SlideMenuCategoryAdapter;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 7/8/16.
 */
public class SlideCategoryLeftPanel extends Fragment implements View.OnClickListener {

    //// UI VARIABLES ///////////////////////
    private View view;
    private RecyclerView rv;
    private CoordinatorLayout overview_coordinator_layout;
    /////////////////////////////////////////
    // NORMAL VARIABLES /////////////////////
    boolean resume;
    ArrayList<AllUtilSClass.Category> al_category;
    ////////////////////////////////////////


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.slide_category_left_panel, container, false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            overview_coordinator_layout = (CoordinatorLayout) view.findViewById(R.id.overview_coordinator_layout);
            getCategories();
        }
        resume = true;
    }


    private void setUI(View view) {
        System.out.println(" WORKING ");

        rv = (RecyclerView) view.findViewById(R.id.rv);
        SlideMenuCategoryAdapter ama = new SlideMenuCategoryAdapter(getActivity(), al_category);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(horizontalLayoutManagaer);
        rv.setAdapter(ama);

       AllUtilMethods.setRVAnimation(rv);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    public void getCategories() {

        HashMap<String, String> map = new HashMap<String, String>();

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        al_category = AllUtilMethods.parseCategories(result);
                        if (al_category.size() > 0) {
                            setUI(view);
                        } else {
                            System.out.println(" Array BLANK ");
                            getActivity().finish();
                        }
                    } else {
                            System.out.println(" ERROR " + pdata.getString("errMsg"));
                            AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("errMsg"));
                            getActivity().finish();
                    }
                } catch (JSONException e) {
                        System.out.println(" JSON EXCEPTION " + e);
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                        getActivity().finish();
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                    System.out.println(" SLIDE LEFT " + msg);
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
                    getActivity().finish();

            }
        }, getActivity()).performPostRequest(getActivity(), AllUtilUrls.base_url + "" + AllUtilUrls.category, map, "", 0);
    }
}