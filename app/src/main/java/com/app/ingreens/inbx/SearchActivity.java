package com.app.ingreens.inbx;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilAdapters.ProductListAdpter;
import com.app.ingreens.inbx.UtilAdapters.TabMenuMostlyViewedAdapter;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by root on 8/2/16.
 */
public class SearchActivity extends MyCustomActivity {
    //////////////////// UI VARIABLES ///////////////////
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private CoordinatorLayout overview_coordinator_layout;
    private TextView toolbar_title;
    private RecyclerView rv;
    private AutoCompleteTextView edt_search;
    private ProgressBar pg;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;
    private String cat_id = "";
    private ArrayList<AllUtilSClass.MostView> al_most_view;
    private Timer timer;
    private boolean timerflag;
    private int page = 1;
    private String next = "false", search_str = "";

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(SearchActivity.this));
        setContentView(R.layout.search_activity);

        overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            setUI();

        }
        resume = true;
    }

    private void setUI() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
            }
        });

        ImageView imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(SearchActivity.this, NonHomePage.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        setToolbarTitle("Search Items");

        pg = (ProgressBar) findViewById(R.id.pg);

        edt_search = (AutoCompleteTextView) findViewById(R.id.edt_search);
        edt_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                pg.setVisibility(View.VISIBLE);
                try {
                    rv.setAdapter(null);
                } catch (Exception e2) {
                    // TODO Auto-generated catch block
                    e2.printStackTrace();
                }

                timerflag = false;
                try {
                    timer.cancel();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                try {
                    timer = new Timer();
                    timer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            if (timerflag) {
                                try {
                                    timer.cancel();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }

                                System.out.println("===addTextChangedListener****" + s.toString());
                                if (s.toString().equalsIgnoreCase(" ")) {

                                } else {
                                    search_str = s.toString();
                                    search(search_str, 0);
                                }
                            } else {
                                timerflag = true;
                            }
                        }
                    }, 0, 1 * 1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        rv = (RecyclerView) findViewById(R.id.rv);
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    if (next.equalsIgnoreCase("true")) {
                        if (AllUtilMethods.isLastItemDisplaying(rv)) {
                            Handler handler = new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    super.handleMessage(msg);
                                    page++;
                                    System.out.println(" END ");
                                    search(search_str, 1, "Loading More ....");
                                }
                            };
                            handler.sendEmptyMessageDelayed(0, 1000);
                        }
                    }
                }
            }
        });
    }

    private void search(String s, final int flag) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", getSharedPreferences("login", 0).getString("customer_id", "0"));
        map.put("searchval", "" + s);
        map.put("page", "" + page);

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                AllUtilMethods.hideKeyboard(SearchActivity.this);
                pg.setVisibility(View.GONE);
                try {
                    System.out.println(" JSON jskdjfkasf " + result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        next = pdata.getString("lastpage");
                        al_most_view = AllUtilMethods.parseSearchList(result, flag, al_most_view);
                        if (al_most_view.size() > 0) {
                            TabMenuMostlyViewedAdapter ama = new TabMenuMostlyViewedAdapter(SearchActivity.this, al_most_view, overview_coordinator_layout);
                            GridLayoutManager lLayout = new GridLayoutManager(SearchActivity.this, 2);
                            rv.setHasFixedSize(true);
                            rv.setLayoutManager(lLayout);
                            rv.setAdapter(ama);
                            AllUtilMethods.setRVAnimation(rv);
                        } else {
                        }
                    } else {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "No Product list found.");
                    }
                } catch (JSONException e) {

                    System.out.println(" JSON EXCEPTION " + e);
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                pg.setVisibility(View.GONE);
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
            }
        }, SearchActivity.this).performPostRequestWithoutDialog(SearchActivity.this, AllUtilUrls.base_url + "" + AllUtilUrls.search_list, map, "", 0);

    }

    private void search(String s, final int flag, String msg) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", getSharedPreferences("login", 0).getString("customer_id", "0"));
        map.put("searchval", "" + s);
        map.put("page", "" + page);

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                pg.setVisibility(View.GONE);
                AllUtilMethods.hideKeyboard(SearchActivity.this);
                try {
                    System.out.println(" JSON jskdjfkasf " + result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        next = pdata.getString("lastpage");
                        al_most_view = AllUtilMethods.parseSearchList(result, flag, al_most_view);
                        if (al_most_view.size() > 0) {
                            rv.getAdapter().notifyDataSetChanged();
//                            TabMenuMostlyViewedAdapter ama = new TabMenuMostlyViewedAdapter(SearchActivity.this, al_most_view,overview_coordinator_layout);
//                            GridLayoutManager lLayout = new GridLayoutManager(SearchActivity.this, 2);
//                            rv.setHasFixedSize(true);
//                            rv.setLayoutManager(lLayout);
//                            rv.setAdapter(ama);
//                            ama.notifyDataSetChanged();
//                            AllUtilMethods.setRVAnimation(rv);
                        } else {
                        }
                    } else {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "No Product list found.");
                    }
                } catch (JSONException e) {
                    System.out.println(" JSON EXCEPTION " + e);
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                }
            }

            @Override
            public void onErrorEncountered(String msg) {

                pg.setVisibility(View.GONE);
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);

            }
        }, SearchActivity.this).performPostRequest(SearchActivity.this, AllUtilUrls.base_url + "" + AllUtilUrls.search_list, map, msg, 0);

    }

    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    /////////////////////// THIS BELOW SEGMENT IS ONLY FOR FRAGMENT HANDLING ????????????????????????????????????????
}
