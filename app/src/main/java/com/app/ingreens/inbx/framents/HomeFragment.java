package com.app.ingreens.inbx.framents;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.ingreens.inbx.AllCategories;
import com.app.ingreens.inbx.MostViewActivity;
import com.app.ingreens.inbx.NonHomePage;
import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.UtilAdapters.SlideMenuCategoryAdapter;
import com.app.ingreens.inbx.UtilAdapters.TabMenuHotdealsAdapter;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by root on 7/8/16.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    //// UI VARIABLES ///////////////////////
    private View view;
    private ViewPager viewPager;
    private ImageView page_text[];
    private LinearLayout count_layout;
    private RecyclerView rv;
    private CoordinatorLayout overview_coordinator_layout;
    private Button btn_all_categories, btn_hot_deal, btn_most_popular;
    /////////////////////////////////////////
    // NORMAL VARIABLES /////////////////////
    private boolean resume;
    private int count = 0;
    private int imgcount = 0, currentPage = 0;
    ArrayList<AllUtilSClass.MainSlideImage> al_main_slide;
    ArrayList<AllUtilSClass.HotDeals> al_hot_deals;
    public Timer swipeTimer;
    ////////////////////////////////////////


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.home_fragment, container, false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            overview_coordinator_layout = (CoordinatorLayout)view.findViewById(R.id.overview_coordinator_layout);
            btn_all_categories = (Button) view.findViewById(R.id.btn_all_categories);
            btn_all_categories.setOnClickListener(this);

            btn_hot_deal = (Button) view.findViewById(R.id.btn_hot_deal);
            btn_hot_deal.setOnClickListener(this);

            btn_most_popular = (Button) view.findViewById(R.id.btn_most_popular);
            btn_most_popular.setOnClickListener(this);

            getMainSlideImage();
        }
        resume = true;
    }

    private void setImageSliderAdapter(){
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);

        ImagePagerAdapter adapter = new ImagePagerAdapter();
        viewPager.setAdapter(adapter);

        count = viewPager.getAdapter().getCount();
        count_layout = (LinearLayout) view.findViewById(R.id.image_count);

        imgcount = viewPager.getAdapter().getCount();
        //System.out.println("Gallery Image Count======>>>" + count+"  "+al_banner.size());

        page_text = new ImageView[imgcount];
        for (int i = 0; i < imgcount; i++) {
            page_text[i] = new ImageView(getActivity());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 8, 0);
            page_text[i].setLayoutParams(lp);
            page_text[i].setImageDrawable(getResources().getDrawable(
                    R.drawable.carousel_indicator_inactive));
            count_layout.addView(page_text[i]);
        }

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                //System.out.println("Item Selected Position=======>>>" + arg0);
                for (int i = 0; i < imgcount; i++) {
                    page_text[i].setImageDrawable(getResources().getDrawable(
                            R.drawable.carousel_indicator_inactive));
                }
                page_text[arg0].setImageDrawable(getResources().getDrawable(
                        R.drawable.carousel_indicator_active));
                currentPage = arg0;
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

        startTiemr();
    }

    private void startTiemr(){
        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == al_main_slide.size()) {
                    currentPage = 0;
                }
                for (int i = 0; i < imgcount; i++) {
                    page_text[i].setImageDrawable(getResources().getDrawable(
                            R.drawable.carousel_indicator_inactive));
                }
                page_text[currentPage].setImageDrawable(getResources().getDrawable(
                        R.drawable.carousel_indicator_active));
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 100, 2000);
    }


    private void setUI(View view) {
        System.out.println(" WORKING ");

        rv = (RecyclerView) view.findViewById(R.id.rv);
        NonHomePage non=(NonHomePage)getActivity();
        TabMenuHotdealsAdapter ama = new TabMenuHotdealsAdapter(getActivity(), al_hot_deals,non.overview_coordinator_layout);

        GridLayoutManager lLayout = new GridLayoutManager(getActivity(), 2);

        rv.setHasFixedSize(true);
        rv.setLayoutManager(lLayout);
        rv.setAdapter(ama);

        AllUtilMethods.setRVAnimation(rv);

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btn_all_categories:
                intent = new Intent(new Intent(getActivity(), AllCategories.class));
                startActivity(intent);
                getActivity().overridePendingTransition( R.anim.slide_up, R.anim.stay );
                swipeTimer.cancel();
                break;
            case R.id.btn_most_popular:
                intent = new Intent(new Intent(getActivity(), MostViewActivity.class));
                startActivity(intent);
                getActivity().overridePendingTransition( R.anim.slide_up, R.anim.stay );
                swipeTimer.cancel();
                break;
        }
    }


    //////////////////////////////////  IMAGE VIEWPAGER ADAPTER /////////////////////////////////////////////
    private class ImagePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return al_main_slide.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((FrameLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Context context = getActivity();
            View itemView = LayoutInflater.from(context).inflate(R.layout.gallery_inflater, container, false);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.imv_gallery);

                // getActivity().finish();
                AllUtilMethods.getImage(getActivity(),R.drawable.placeholder,al_main_slide.get(position).image,imageView,800,400);
            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((FrameLayout) object);
        }
    }

    public  void getMainSlideImage(){

        HashMap<String,String> map=new HashMap<String, String>();

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ",result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if(pdata.getString("success").equalsIgnoreCase("true")){
                        al_main_slide= AllUtilMethods.parseMainSlideImage(result);
                        if(al_main_slide.size()>0){
                            setImageSliderAdapter();
                            getHotDeals();
                        }else{
                           getActivity().finish();
                        }
                    }else{
                            // getActivity().finish();
                            getActivity().finish();
                            AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("errMsg"));
                    }
                } catch (JSONException e) {
                        System.out.println(" JSON EXCEPTION "+e);
                        getActivity().finish();
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout,"Sorry! Data retrieve error has occurred, please try later.");
                }
            }
            @Override
            public void onErrorEncountered(String msg) {
                    getActivity().finish();
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout,msg);

            }
        },getActivity()).performPostRequest(getActivity(), AllUtilUrls.base_url+""+AllUtilUrls.main_slider,map,"",0);
    }

    public  void getHotDeals(){

        HashMap<String,String> map=new HashMap<String, String>();
        map.put("customer_id", getActivity().getSharedPreferences("login", 0).getString("customer_id", "0"));

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ",result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if(pdata.getString("success").equalsIgnoreCase("true")){
                        al_hot_deals= AllUtilMethods.parseHotDeals(result);
                        if(al_hot_deals.size()>0){
                            setUI(view);
                        }else{
                           // getActivity().finish();
                        }
                    }else{
                        //getActivity().finish();
                            AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("errMsg"));
                    }
                } catch (JSONException e) {
                        System.out.println(" JSON EXCEPTION "+e);
                        // getActivity().finish();
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout,"Sorry! Data retrieve error has occurred, please try later.");
                }
            }
            @Override
            public void onErrorEncountered(String msg) {
                    // getActivity().finish();
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout,msg);

            }
        },getActivity()).performPostRequest(getActivity(), AllUtilUrls.base_url+""+AllUtilUrls.hot_deal,map,"",0);
    }

    @Override
    public void onStop() {
        super.onStop();
        swipeTimer.cancel();
    }
}