package com.app.ingreens.inbx.UtilAdapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.ProductDetailsActivity;
import com.app.ingreens.inbx.ProductListActivity;
import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.SubCategory;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;

import java.util.List;

/**
 * Created by Papun's on 7/23/2016.
 */
public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder> {

    private List<AllUtilSClass.SubCategory> moviesList;
    Activity ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_category;
        public ImageView imv_category;

        public MyViewHolder(View view) {
            super(view);
            tv_category = (TextView) view.findViewById(R.id.tv_category);
            imv_category = (ImageView) view.findViewById(R.id.imv_category);
        }
    }

    public SubCategoryAdapter(Activity ctx, List<AllUtilSClass.SubCategory> moviesList) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_category_list_cell, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_category.setText(moviesList.get(position).name);
        holder.tv_category.setSelected(true);

        AllUtilMethods.getImage(ctx, R.drawable.subcategory_placeholder, moviesList.get(position).image, holder.imv_category, 300, 300);


        holder.imv_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(ctx, ProductListActivity.class));
                intent.putExtra("cat_id", moviesList.get(position).id);
                ctx.startActivity(intent);
                ctx.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
