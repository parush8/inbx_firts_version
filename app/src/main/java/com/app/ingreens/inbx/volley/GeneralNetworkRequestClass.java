package com.app.ingreens.inbx.volley;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.Spanned;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

public class GeneralNetworkRequestClass {

	//private CustomDialog pd;
	private ProgressDialog pd;
	private CommonCallback callback;
	Context ctx;

	public static boolean showProgressDilog=true;

	public GeneralNetworkRequestClass(CommonCallback callback,Context ctx)
	{
		this.callback=callback;
		this.ctx=ctx;
	}


	public void performPostRequest(Context mContext, final String url, final HashMap<String,String> values, String msg, int cancel)
	{
		StringRequest req=new StringRequest(Request.Method.POST, url, new Listener<String>() {
			
			@Override
			public void onResponse(String result) {
				// TODO Auto-generated method stub
				Log.v("General Network class", result);
				Log.v("URL & PARAMS", url+"  "+values.toString());
				dismissDialog();
				callback.onNetworkProcessingCompleted(result);
			}

		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				dismissDialog();
				String mssg="";
				if (error instanceof TimeoutError || error instanceof NoConnectionError) 
				{
					mssg="Sorry ! connection error";
				} else if (error instanceof AuthFailureError) {
					//TODO
					mssg="Sorry ! some rrror has occured";
				} else if (error instanceof ServerError) {
					//TODO
					mssg="Sorry ! some error has occured";
				} else if (error instanceof NetworkError) {
					//TODO
					mssg="Sorry ! connection error";
				} else if (error instanceof ParseError) {
					//TODO
					mssg="Sorry ! some error has occured";
				}
				callback.onErrorEncountered(mssg);
			}
		}){
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub

				HashMap<String,String> params=values;
				Log.e("Params Values", params.toString()+" "+url);
				return params;
			}
		};
		showProgressDialog(mContext,msg,cancel);
		req.setShouldCache(false);
		int socketTimeout = 60000;//60 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
		GlobalVariable.getInstance(mContext).addToRequestQueue(req, GlobalVariable.TAG);
	}

	public void performPostRequestWithoutDialog(Context mContext, final String url, final HashMap<String,String> values, String msg, int cancel)
	{
		StringRequest req=new StringRequest(Request.Method.POST, url, new Listener<String>() {

			@Override
			public void onResponse(String result) {
				// TODO Auto-generated method stub
				Log.v("General Network class", result);
				Log.v("URL & PARAMS", url+"  "+values.toString());
				callback.onNetworkProcessingCompleted(result);
			}

		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				String mssg="";
				if (error instanceof TimeoutError || error instanceof NoConnectionError)
				{
					mssg="Sorry ! connection error";
				} else if (error instanceof AuthFailureError) {
					//TODO
					mssg="Sorry ! some rrror has occured";
				} else if (error instanceof ServerError) {
					//TODO
					mssg="Sorry ! some error has occured";
				} else if (error instanceof NetworkError) {
					//TODO
					mssg="Sorry ! connection error";
				} else if (error instanceof ParseError) {
					//TODO
					mssg="Sorry ! some error has occured";
				}
				callback.onErrorEncountered(mssg);
			}
		}){
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub

				HashMap<String,String> params=values;
				Log.e("Params Values", params.toString()+" "+url);
				return params;
			}
		};
		req.setShouldCache(false);
		int socketTimeout = 60000;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		req.setRetryPolicy(policy);
		GlobalVariable.getInstance(mContext).addToRequestQueue(req, GlobalVariable.TAG);
	}
	
	public void performPostRequest(Context mContext,String url,final HashMap<String,String> values,Spanned msg,int cancel)
	{
		StringRequest req=new StringRequest(Request.Method.POST, url, new Listener<String>() {

			@Override
			public void onResponse(String result) {
				// TODO Auto-generated method stub
				Log.v("General Network class", result);
				dismissDialog();
				callback.onNetworkProcessingCompleted(result);
			}

		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				dismissDialog();
				String mssg="";
				if (error instanceof TimeoutError || error instanceof NoConnectionError) 
				{
					mssg="This app requires an internet connection.Make sure you are connected to a wifi network or have switched on your network data.";
				} else if (error instanceof AuthFailureError) {
					//TODO
					mssg="Sorry ! some error has occured";
				} else if (error instanceof ServerError) {
					//TODO
					mssg="Sorry ! some error has occured";
				} else if (error instanceof NetworkError) {
					//TODO
					mssg="Sorry ! connection error";
				} else if (error instanceof ParseError) {
					//TODO
					mssg="Sorry ! some error has occured";
				}
				callback.onErrorEncountered(mssg);
			}
		}){
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub

				HashMap<String,String> params=values;
				Log.v("Params Values", params.toString());
				return params;
			}
		};
		showProgressDialog(mContext,msg,cancel);
		GlobalVariable.getInstance(mContext).addToRequestQueue(req, GlobalVariable.TAG);
	}


	public void performGetRequest(Context mContext,String url,String msg,int cancel)
	{
		StringRequest req=new StringRequest(Request.Method.GET, url, new Listener<String>() {

			@Override
			public void onResponse(String result) {
				// TODO Auto-generated method stub
				dismissDialog();
				callback.onNetworkProcessingCompleted(result);
			}
		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				dismissDialog();
				String mssg="";
				if (error instanceof TimeoutError || error instanceof NoConnectionError) 
				{
					mssg="This app requires an internet connection.Make sure you are connected to a wifi network or have switched on your network data.";
				} else if (error instanceof AuthFailureError) {
					//TODO
					mssg="Sorry ! some error has occured";
				} else if (error instanceof ServerError) {
					//TODO
					mssg="Sorry ! some error has occured";
				} else if (error instanceof NetworkError) {
					//TODO
					mssg="Sorry ! connection error";
				} else if (error instanceof ParseError) {
					//TODO
					mssg="Sorry ! some error has occured";
				}
				callback.onErrorEncountered(mssg);
			}
		});
		if(cancel==1){
			
		}else{
			showProgressDialog(mContext,msg,cancel);
		}
		GlobalVariable.getInstance(mContext).addToRequestQueue(req, GlobalVariable.TAG);
	}



	public void performMultipartPostRequest(Context mContext,String url,final HashMap<String,String> values,final HashMap<String,File> files,String msg,int cancel)
	{
		System.out.println(" VALUES "+values);
		MultipartVolleyRequest req=new MultipartVolleyRequest(mContext,Request.Method.POST, url, new Listener<String>() {

			@Override
			public void onResponse(String result) {
				// TODO Auto-generated method stub
				dismissDialog();
				callback.onNetworkProcessingCompleted(result);
			}
		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				dismissDialog();
				String mssg="";
				if (error instanceof TimeoutError || error instanceof NoConnectionError) 
				{
					mssg="This app requires an internet connection.Make sure you are connected to a wifi network or have switched on your network data.";
				} else if (error instanceof AuthFailureError) {
					//TODO
					mssg="Sorry ! some error has occured";
				} else if (error instanceof ServerError) {
					//TODO
					mssg="Sorry ! server error encountered";
				} else if (error instanceof NetworkError) {
					//TODO
					mssg="Sorry ! connection error";
				} else if (error instanceof ParseError) {
					//TODO
					mssg="Sorry ! some error has occured";
				}
				callback.onErrorEncountered(mssg);
			}
		},files,values);
		showProgressDialog(mContext,msg,cancel);
		GlobalVariable.getInstance(mContext).addToRequestQueue(req, GlobalVariable.TAG);
	}

	private void showProgressDialog(Context context,String msg,int cancel)
	{
		if(pd==null)
		{
			//pd=new CustomDialog(ctx,msg,cancel);
			pd=new ProgressDialog(ctx);
			pd.setCancelable(false);
			pd.setMessage(msg);
			pd.show();
		}

	}
	
	private void showProgressDialog(Context context,Spanned msg,int cancel)
	{
		if(pd==null)
		{
			//pd=new CustomDialog(ctx,msg,cancel);
			pd=new ProgressDialog(ctx);
			pd.setMessage(msg);
			pd.setCancelable(false);
			pd.show();
		}

	}


	private void dismissDialog()
	{
		if(pd!=null&&pd.isShowing())
		{
			pd.dismiss();
			pd=null;
		}
	}

	public void performPostRequestForSearch(Context mContext,String url,final HashMap<String,String> values,String msg,int cancel)
	{
		StringRequest req=new StringRequest(Request.Method.POST, url, new Listener<String>() {
          
			@Override
			public void onResponse(String result) {
				// TODO Auto-generated method stub
				Log.v("General Network class", result);
				dismissDialog();
				callback.onNetworkProcessingCompleted(result);
			}

		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				dismissDialog();
				String mssg="";
				if (error instanceof TimeoutError || error instanceof NoConnectionError) 
				{
					mssg="This app requires an internet connection.Make sure you are connected to a wifi network or have switched on your network data.";
				} else if (error instanceof AuthFailureError) {
					//TODO
					mssg="Sorry ! some error has occured";
				} else if (error instanceof ServerError) {
					//TODO
					mssg="Sorry ! some error has occured";
				} else if (error instanceof NetworkError) {
					//TODO
					mssg="Sorry ! connection error";
				} else if (error instanceof ParseError) {
					//TODO
					mssg="Sorry ! some error has occured";
				}
				callback.onErrorEncountered(mssg);
			}
		}){
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub

				HashMap<String,String> params=values;
				Log.v("Params Values", params.toString());
				return params;
			}
		};
		GlobalVariable.getInstance(mContext).addToRequestQueue(req, GlobalVariable.TAG);
	}
}
