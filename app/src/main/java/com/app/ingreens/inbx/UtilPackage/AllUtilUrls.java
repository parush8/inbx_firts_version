package com.app.ingreens.inbx.UtilPackage;

/**
 * Created by root on 7/20/16.
 */
public class AllUtilUrls {

    // SERVER KEY AIzaSyD9sL9pwObUv_qZ5KfrefJNRCVhWsr8sLo

//    Project ID
//    inbx-96628
//    Project number
//    687679305422

    public static String SENDER_ID="687679305422";
    public static int MAX_CART_NUMBER=99;

    public static String base_url="http://104.236.235.220/mkart/nonagri/webservice/index.php?";

    public static String login="type=cart&method=login";
    public static String category="type=cart&method=category";
    public static String sub_category="type=cart&method=subcategory";
    public static String main_slider="type=cart&method=mainslider";
    public static String hot_deal="type=cart&method=hotdeal";
    public static String most_view="type=cart&method=mostview";
    public static String product_lis="type=productlist&catid=";
    public static String product_details="type=productinfo";
    public static String add_to_cart="type=cart&method=cartproductadd";
    public static String get_cart_list="type=cart&method=cartproductlist";
    public static String update_cart_item="type=cart&method=cartproductupdate";
    public static String remove_cart_item="type=cart&method=cartproductremove";
    public static String place_order="type=cart&method=placeorder";
    public static String orderlist="type=cart&method=orderlist";
    public static String search_list="type=cart&method=search";
    public static String coupon_code="type=cart&method=couponcode";
    public static String order_details="type=cart&method=orderdetails";
    public static String about_us="http://104.236.235.220/mkart/agri/html/aboutus.html";
    public static String contact_us="http://104.236.235.220/mkart/agri/html/contactus.html";
    public static String cat_id="type=cart&method=brand";
    public static String advance_search="type=cart&method=advance";
    public static String change_psw="type=cart&method=changepassword";
    public static String myaccount="type=cart&method=myaccount";
    public static String forgetpassword="type=cart&method=forgetpassword";
}
