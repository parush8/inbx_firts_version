package com.app.ingreens.inbx.volley;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class GlobalVariable {

	public static final String TAG = GlobalVariable.class
			.getSimpleName();
	private RequestQueue mRequestQueue;

	private static GlobalVariable mInstance;
	private static Context mContext;




	private GlobalVariable(){}
	public static synchronized GlobalVariable getInstance(Context context) {
		if(mInstance==null)
		{
			mInstance=new GlobalVariable();
		}
		if(mContext==null)
		{
			mContext=context;
		}
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(mContext);
		}
		return mRequestQueue;
	}

	

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
			System.out.println(" cancel tag ");
		}
	}	
}
