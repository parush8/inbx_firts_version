package com.app.ingreens.inbx.UtilPackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ingreens.inbx.AdvanceSearchFilterActivity;
import com.app.ingreens.inbx.InBxApplication;
import com.app.ingreens.inbx.MyCartActivity;
import com.app.ingreens.inbx.MyCustomActivity;
import com.app.ingreens.inbx.NonHomePage;
import com.app.ingreens.inbx.NonLoginActivity;
import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.TrackYourOrderActivity;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by root on 7/8/16.
 */
public class AllUtilMethods {

    public static void disableMenuItemForSometimes(final MenuItem item) {
        item.setEnabled(false);
        Handler h = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                item.setEnabled(true);
            }
        };
        h.postDelayed(r, 2000);
    }

    public static void CreateSnackBar(View v, String str) {
        Snackbar snackbar = Snackbar
                .make(v, str, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View view = snackbar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
//        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)view.getLayoutParams();
//        params.gravity = Gravity.TOP;
//        view.setLayoutParams(params);
        snackbar.show();
    }

    public static void getImage(Context ctx, int int_drawable, String img_url, ImageView imv, int width, int height) {
//        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
//                .cacheOnDisk(true).cacheInMemory(true)
//                .imageScaleType(ImageScaleType.EXACTLY)
//                .displayer(new FadeInBitmapDisplayer(300)).build();
//
//        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
//                ctx)
//                .defaultDisplayImageOptions(defaultOptions)
//                .memoryCache(new WeakMemoryCache())
//                .diskCacheSize(100 * 1024 * 1024).build();
//
//        ImageLoader.getInstance().init(config);
//
//        ImageLoader imageLoader = ImageLoader.getInstance();
//        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
//                .cacheOnDisk(true).resetViewBeforeLoading(true)
//                .showImageForEmptyUri(int_drawable)
//                .showImageOnFail(int_drawable)
//                .showImageOnLoading(int_drawable)
////                .postProcessor(new BitmapProcessor() {
////                    @Override
////                    public Bitmap process(Bitmap bmp) {
////                        return Bitmap.createScaledBitmap(bmp, 800, 300, false);
////                    }
////                })
//                        .build();
//
//        imageLoader.displayImage(url.trim(), imv, options);

        try {
            URL url = new URL(img_url.trim());
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            Picasso.with(ctx)
                    .load("" + uri)
                    .error(int_drawable)
                    .resize(width, height)
                    .into(imv);
        } catch (Exception e) {
            Picasso.with(ctx)
                    .load(int_drawable)
                    .into(imv);
        }
    }

//    public static void problemOccurDialog( final Activity ctx){
//        final Dialog dialog = new Dialog(ctx,android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.error);
//        Window window = dialog.getWindow();
//        WindowManager.LayoutParams wlp = window.getAttributes();
//
//        wlp.gravity = Gravity.CENTER;
//        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
//        window.setAttributes(wlp);
//        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        dialog.show();
//
//        Button btn_ok=(Button)dialog.findViewById(R.id.btn_ok);
//        btn_ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.cancel();
//                ctx.finish();
//                System.exit(10);
//            }
//        });
//    }

    public static void emptyCartDialog(final Activity ctx) {
        final Dialog dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.empty_cart_dialog);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
    }

    public static void appCloseDialog(String msg, final Activity ctx) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ctx);
        alertDialogBuilder.setTitle("INBx");
        alertDialogBuilder
                .setMessage("" + msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // NonHomePage nh= (NonHomePage)ctx;
                        //nh.stopTimer(1);
//                        Intent startMain = new Intent(Intent.ACTION_MAIN);
//                        startMain.addCategory(Intent.CATEGORY_HOME);
//                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        ctx.startActivity(startMain);
//                        dialog.dismiss();
                        Intent intent = new Intent(new Intent(ctx, NonLoginActivity.class));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        ctx.startActivity(intent);
                        ctx.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                        dialog.dismiss();
                        ctx.finish();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public static void networkDialog(String msg, final Activity ctx)  {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ctx);
        alertDialogBuilder.setTitle("INBx");
        alertDialogBuilder
                .setMessage("" + msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ctx.startActivity(startMain);
                        dialog.dismiss();
                        ctx.finish();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }


    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            // Ignore exceptions if any
            Log.e("KeyBoardUtil", e.toString(), e);
        }
    }

    public static boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    public static int login(final Activity ctx, String email, String password, final CoordinatorLayout overview_coordinator_layout, String gecm_regid) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", email);
        map.put("password", password);
        map.put("gcm_regid", gecm_regid);

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");

                    if (status.getString("success").equalsIgnoreCase("true")) {
                        JSONObject list_item = pdata.getJSONObject("listItem");
                        ctx.getSharedPreferences("login", 0).edit().putString("log", "yes").commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("email", list_item.getString("email")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("customer_id", list_item.getString("customer_id")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("firstname", list_item.getString("firstname")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("lastname", list_item.getString("lastname")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("group_id", list_item.getString("group_id")).commit();

                        ctx.getSharedPreferences("login", 0).edit().putString("city", list_item.getString("city")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("street_address", list_item.getString("street_address")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("postcode", list_item.getString("postcode")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("telephone", list_item.getString("telephone")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("cart_count", list_item.getString("totqty")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("shoppingcartid", list_item.getString("shoppingCartId")).commit();
                        Intent intent = new Intent(new Intent(ctx, NonHomePage.class));
                        ctx.startActivity(intent);
                        ctx.overridePendingTransition(R.anim.right_in, R.anim.left_out);

                    } else {

                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("errMsg"));
                        ctx.getSharedPreferences("login", 0).edit().putString("log", "no").commit();

                    }
                } catch (JSONException e) {
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                    ctx.getSharedPreferences("login", 0).edit().putString("log", "no").commit();
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
                ctx.getSharedPreferences("login", 0).edit().putString("log", "no").commit();
            }
        }, ctx).performPostRequest(ctx, AllUtilUrls.base_url + "" + AllUtilUrls.login, map, "Please wait ...", 0);
        return 1;
    }


    public static ArrayList<AllUtilSClass.Category> parseCategories(String result) {
        ArrayList<AllUtilSClass.Category> al_category = new ArrayList<AllUtilSClass.Category>();
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONObject listItem = pdata.getJSONObject("listItem");
            JSONArray category = listItem.getJSONArray("category");
            AllUtilSClass a = new AllUtilSClass();
            for (int i = 0; i < category.length(); i++) {
                AllUtilSClass.Category cat = a.new Category();
                JSONObject cat_obj = category.getJSONObject(i);
                cat.id = cat_obj.getString("id");
                cat.name = cat_obj.getString("name");
                cat.thumb = cat_obj.getString("thumb");
                cat.image = cat_obj.getString("image");
                al_category.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return al_category;
    }

    public static ArrayList<AllUtilSClass.SubCategory> parseSubCategories(String result) {
        ArrayList<AllUtilSClass.SubCategory> al_sub_category = new ArrayList<AllUtilSClass.SubCategory>();
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONObject list_item = pdata.getJSONObject("listItem");
            JSONArray sub_category = list_item.getJSONArray("subcategory");
            AllUtilSClass a = new AllUtilSClass();
            for (int i = 0; i < sub_category.length(); i++) {
                AllUtilSClass.SubCategory cat = a.new SubCategory();
                JSONObject cat_obj = sub_category.getJSONObject(i);
                cat.id = cat_obj.getString("id");
                cat.name = cat_obj.getString("name");
                cat.image = cat_obj.getString("image");
                al_sub_category.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return al_sub_category;
    }

    public static ArrayList<AllUtilSClass.MainSlideImage> parseMainSlideImage(String result) {
        ArrayList<AllUtilSClass.MainSlideImage> al_sub_category = new ArrayList<AllUtilSClass.MainSlideImage>();
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONArray sub_category = pdata.getJSONArray("listItem");
            AllUtilSClass a = new AllUtilSClass();
            for (int i = 0; i < sub_category.length(); i++) {
                AllUtilSClass.MainSlideImage cat = a.new MainSlideImage();
                JSONObject cat_obj = sub_category.getJSONObject(i);
                cat.title = cat_obj.getString("title");
                cat.image = cat_obj.getString("image");
                al_sub_category.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return al_sub_category;
    }

    public static ArrayList<AllUtilSClass.HotDeals> parseHotDeals(String result) {
        ArrayList<AllUtilSClass.HotDeals> al_sub_category = new ArrayList<AllUtilSClass.HotDeals>();
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONArray sub_category = pdata.getJSONArray("listItem");
            AllUtilSClass a = new AllUtilSClass();
            for (int i = 0; i < sub_category.length(); i++) {
                AllUtilSClass.HotDeals cat = a.new HotDeals();
                JSONObject cat_obj = sub_category.getJSONObject(i);
                cat.name = cat_obj.getString("name");
                cat.id = cat_obj.getString("id");
                cat.image_url = cat_obj.getString("image_url");
                cat.rgprice = cat_obj.getString("rgprice");
                cat.spprice = cat_obj.getString("spprice");
                cat.tierPrices = cat_obj.getString("tierPrices");
                cat.stock_status = cat_obj.getString("stock_status");
                al_sub_category.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return al_sub_category;
    }

    public static ArrayList<AllUtilSClass.MostView> parseMostViewList(String result) {
        ArrayList<AllUtilSClass.MostView> al_sub_category = new ArrayList<AllUtilSClass.MostView>();
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONArray sub_category = pdata.getJSONArray("listItem");
            AllUtilSClass a = new AllUtilSClass();
            for (int i = 0; i < sub_category.length(); i++) {
                AllUtilSClass.MostView cat = a.new MostView();
                JSONObject cat_obj = sub_category.getJSONObject(i);
                cat.name = cat_obj.getString("name");
                cat.id = cat_obj.getString("id");
                cat.image_url = cat_obj.getString("image_url");
                cat.rgprice = cat_obj.getString("rgprice");
                cat.spprice = cat_obj.getString("spprice");
                cat.tierPrices = cat_obj.getString("tierPrices");
                cat.url = cat_obj.getString("url");
                cat.stock_status = cat_obj.getString("stock_status");
                al_sub_category.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return al_sub_category;
    }

    public static ArrayList<AllUtilSClass.MostView> parseSearchList(String result, int flag, ArrayList<AllUtilSClass.MostView> al_most_view) {
        if (flag == 0) {
            al_most_view = new ArrayList<AllUtilSClass.MostView>();
        }

        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONArray sub_category = pdata.getJSONArray("listItem");
            AllUtilSClass a = new AllUtilSClass();
            for (int i = 0; i < sub_category.length(); i++) {
                AllUtilSClass.MostView cat = a.new MostView();
                JSONObject cat_obj = sub_category.getJSONObject(i);
                cat.name = cat_obj.getString("name");
                cat.id = cat_obj.getString("id");
                cat.image_url = cat_obj.getString("image_url");
                cat.rgprice = cat_obj.getString("rgprice");
                cat.spprice = cat_obj.getString("spprice");
                cat.tierPrices = cat_obj.getString("tierPrices");
                cat.stock = cat_obj.getString("stock");
                cat.stock_status = cat_obj.getString("stock_status");
                al_most_view.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return al_most_view;
    }

    // ArrayList<AllUtilSClass.ProductList> al_sub_category=new ArrayList<AllUtilSClass.ProductList>();
    public static ArrayList<AllUtilSClass.ProductList> parseProductList(String result, int flag, ArrayList<AllUtilSClass.ProductList> al_product_list) {
        if (flag == 0) {
            al_product_list = new ArrayList<AllUtilSClass.ProductList>();
        }

        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONArray sub_category = pdata.getJSONArray("listItem");
            AllUtilSClass a = new AllUtilSClass();
            for (int i = 0; i < sub_category.length(); i++) {
                AllUtilSClass.ProductList cat = a.new ProductList();
                JSONObject cat_obj = sub_category.getJSONObject(i);
                cat.name = cat_obj.getString("name");
                cat.product_id = cat_obj.getString("product_id");
                cat.image_url = cat_obj.getString("image_url");
                cat.brand = cat_obj.getString("brand");
                cat.rgprice = cat_obj.getString("rgprice");
                cat.fltprice = cat_obj.getString("fltprice");
                //cat.sku=cat_obj.getString("sku");
                cat.spprice = cat_obj.getString("spprice");
                cat.status = cat_obj.getString("status");
                cat.tierPrices = cat_obj.getString("tierPrices");
                cat.stock = cat_obj.getString("stock");
                cat.stock_status = cat_obj.getString("stock_status");
                // cat.type=cat_obj.getString("type");

                al_product_list.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return al_product_list;
    }

    public static AllUtilSClass.ProductDetails parseProductDetails(String result) {
        AllUtilSClass a = new AllUtilSClass();
        AllUtilSClass.ProductDetails pd = a.new ProductDetails();
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            pd.description = status.getString("description");
            pd.hot_deal = status.getString("hot_deal");
            pd.name = status.getString("name");
            pd.rgprice = status.getString("rgprice");
            pd.product_id = status.getString("product_id");
            pd.image_url = status.getString("image_url");
            pd.set = status.getString("set");
            pd.short_description = status.getString("short_description");
            pd.sku = status.getString("sku");
            pd.spprice = status.getString("spprice");
            pd.status = status.getString("status");
            pd.stock = status.getString("stock");
            pd.stock_status = status.getString("stock_status");
            pd.type_id = status.getString("type_id");
            pd.type = status.getString("type");

            JSONArray j_img_gall = status.getJSONArray("image_gallery");
            if (j_img_gall.length() > 0) {
                for (int i = 0; i < j_img_gall.length(); i++) {
                    JSONObject j = j_img_gall.getJSONObject(i);
                    AllUtilSClass.ProductDetailsImageUrl p = a.new ProductDetailsImageUrl();
                    p.image = j.getString("image");
                    p.position = j.getString("position");
                    pd.al_image_url.add(p);
                }
            }
            JSONArray j_relativeProduct = status.getJSONArray("relativeProduct");
            if (j_relativeProduct.length() > 0) {
                for (int i = 0; i < j_relativeProduct.length(); i++) {
                    JSONObject j = j_relativeProduct.getJSONObject(i);
                    AllUtilSClass.ProductDetailsSimilarProductImageUrl p = a.new ProductDetailsSimilarProductImageUrl();
                    p.id = j.getString("id");
                    p.imageurl = j.getString("imageurl");
                    p.name = j.getString("name");
                    p.rgprice = j.getString("rgprice");
                    p.spprice = j.getString("spprice");
                    pd.al_similar_product.add(p);
                }
            }
            JSONArray j_upsellProduct = status.getJSONArray("upsellProduct");
            if (j_upsellProduct.length() > 0) {
                for (int i = 0; i < j_upsellProduct.length(); i++) {
                    JSONObject j = j_upsellProduct.getJSONObject(i);
                    AllUtilSClass.ProductDetailsSimilarProductImageUrl p = a.new ProductDetailsSimilarProductImageUrl();
                    p.id = j.getString("id");
                    p.imageurl = j.getString("imageurl");
                    p.name = j.getString("name");
                    p.rgprice = j.getString("rgprice");
                    p.spprice = j.getString("spprice");
                    pd.al_upsell_product.add(p);
                }
            }
            JSONArray j_tirePrices = status.getJSONArray("tierPrices");
            if (j_tirePrices.length() > 0) {
                for (int i = 0; i < j_tirePrices.length(); i++) {
                    JSONObject j = j_tirePrices.getJSONObject(i);
                    AllUtilSClass.ProductDetailsTierPrices p = a.new ProductDetailsTierPrices();
                    p.info = j.getString("info");
                    pd.al_tier_price.add(p);
                }
            }

        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return pd;
    }

    public static int addToCart(final Activity ctx, String shoppingcartid, String customer_id, String product_id, String qty, final CoordinatorLayout overview_coordinator_layout, final int activity_status) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", customer_id);
        map.put("product_id", product_id);
        map.put("shoppingcartid", shoppingcartid);
        map.put("qty", qty);
        final int[] res = {0};

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");

                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        ctx.getSharedPreferences("login", 0).edit().putString("shoppingcartid", pdata.getString("shoppingcartid")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("cart_count", pdata.getJSONObject("info").getString("totqty")).commit();
                        MyCustomActivity ma = (MyCustomActivity) ctx;
                        ma.onCartItemUpdate();
                        if (activity_status == 1) {
                            Intent intent = new Intent(new Intent(ctx, MyCartActivity.class));
                            ctx.startActivity(intent);
                            ctx.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                        } else {

                            AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Item has been successfully added to cart.");


                        }
                        res[0] = 1;
                    } else {

                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, status.getJSONObject("errNode").getString("errMsg"));

                    }
                } catch (JSONException e) {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");

                }
            }

            @Override
            public void onErrorEncountered(String msg) {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);


            }
        }, ctx).performPostRequest(ctx, AllUtilUrls.base_url + "" + AllUtilUrls.add_to_cart, map, "Please wait ...", 0);
        return res[0];
    }

    public static AllUtilSClass.MyCartDetails parseCartList(String result, Activity act) {
        AllUtilSClass.MyCartDetails mc = null;
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONArray sub_category = pdata.getJSONArray("listItem");
            JSONObject info = pdata.getJSONObject("info");
            AllUtilSClass a = new AllUtilSClass();
            mc = a.new MyCartDetails();
            mc.shipping_info = pdata.getString("shipinfo");
            mc.grandTotal = info.getString("grandtotal");
            mc.totalQty = info.getString("totqty");
            act.getSharedPreferences("login", 0).edit().putString("cart_count", info.getString("totqty")).commit();
            mc.discount = info.getString("discount");
            mc.couponCode = info.getString("couponCode");
            mc.subtotal = info.getString("subtotal");
            for (int i = 0; i < sub_category.length(); i++) {
                AllUtilSClass.MyCartItems cat = a.new MyCartItems();
                JSONObject cat_obj = sub_category.getJSONObject(i);
                cat.name = cat_obj.getString("name");
                cat.imageurl = cat_obj.getString("imageurl");
                cat.price = cat_obj.getString("price");
                cat.productid = cat_obj.getString("productId");
                cat.qty = cat_obj.getString("qty");
                mc.al_cart_items.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return mc;
    }


    public static int updateCart(final Activity ctx, String shoppingcartid, String customer_id, String product_id, String qty, final CoordinatorLayout overview_coordinator_layout, final MyCartGetDataCallback my_cartinterface) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", customer_id);
        map.put("product_id", product_id);
        map.put("shoppingcartid", shoppingcartid);
        map.put("qty", qty);
        final int[] res = {0};

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");

                    if (pdata.getString("success").equalsIgnoreCase("true")) {

                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "" + pdata.getString("successMsg"));
                        ctx.getSharedPreferences("login", 0).edit().putString("cart_count", pdata.getJSONObject("info").getString("totqty")).commit();
                        my_cartinterface.callMyCartList();
                        res[0] = 1;
                        MyCustomActivity ma = (MyCustomActivity) ctx;
                        ma.onCartItemUpdate();


                    } else {

                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, status.getJSONObject("errNode").getString("errMsg"));


                        res[0] = 0;
                    }
                } catch (JSONException e) {
                    System.out.println(" JSON EXCEPTION " + e);

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");

                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                res[0] = 0;

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);


            }
        }, ctx).performPostRequest(ctx, AllUtilUrls.base_url + "" + AllUtilUrls.update_cart_item, map, "Please wait ...", 0);
        return res[0];
    }

    public static int removeCartItem(final Activity ctx, String shoppingcartid, String customer_id, String product_id, final CoordinatorLayout overview_coordinator_layout, final MyCartGetDataCallback my_cartinterface) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", customer_id);
        map.put("product_id", product_id);
        map.put("shoppingcartid", shoppingcartid);

        final int[] res = {0};

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");

                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        // ctx. getSharedPreferences("login", 0).edit().putString("shoppingcartid", pdata.getString("shoppingcartid")).commit();

                        ctx.getSharedPreferences("login", 0).edit().putString("cart_count", pdata.getJSONObject("info").getString("totqty")).commit();
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Item has been successfully removed from cart.");
                        my_cartinterface.removeMyCartList();
                        res[0] = 1;
                        MyCustomActivity ma = (MyCustomActivity) ctx;
                        ma.onCartItemUpdate();

                    } else {

                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, status.getJSONObject("errNode").getString("errMsg"));


                    }
                } catch (JSONException e) {

                    System.out.println(" JSON EXCEPTION " + e);
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");


                }
            }

            @Override
            public void onErrorEncountered(String msg) {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);

            }
        }, ctx).performPostRequest(ctx, AllUtilUrls.base_url + "" + AllUtilUrls.remove_cart_item, map, "Please wait ...", 0);
        return res[0];
    }

    public interface MyCartGetDataCallback {
        public void callMyCartList();
        public void removeMyCartList();
    }

    public interface OnPaymentCompleteCallback {
        public void onCompletePAyment();
    }

    public static void showPaymentDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_mode_dialog);

        Button btn_online_paymeny = (Button) dialog.findViewById(R.id.btn_online_paymeny);
        btn_online_paymeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btn_cod = (Button) dialog.findViewById(R.id.btn_cod);
        btn_cod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    public static int placeOrder(final Activity ctx,
                                 String shoppingcartid,
                                 String customer_id,
                                 String mode,
                                 String firstname,
                                 String lastname,
                                 String email,
                                 String street,
                                 String city,
                                 String postcode,
                                 String telephone,
                                 String company,
                                 String sfirstname,
                                 String slastname,
                                 String scompany,
                                 String sstreet,
                                 String scity,
                                 String spostcode,
                                 String stelephone,
                                 final CoordinatorLayout overview_coordinator_layout,
                                 final OnPaymentCompleteCallback my_cartinterface) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", customer_id);
        map.put("shoppingcartid", shoppingcartid);
        map.put("mode", mode);

        map.put("firstname", firstname);
        map.put("lastname", lastname);
        map.put("email", email);
        map.put("street", street);
        map.put("city", city);
        map.put("postcode", postcode);
        map.put("telephone", telephone);
        map.put("company", company);

        map.put("sfirstname", sfirstname);
        map.put("slastname", slastname);
        map.put("scompany", scompany);
        map.put("sstreet", sstreet);
        map.put("scity", scity);
        map.put("spostcode", spostcode);
        map.put("stelephone", stelephone);

        final int[] res = {0};

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");

                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        // ctx. getSharedPreferences("login", 0).edit().putString("shoppingcartid", pdata.getString("shoppingcartid")).commit();

                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("successMsg"));
                        ctx.getSharedPreferences("login", 0).edit().putString("cart_count", "0").commit();
                        my_cartinterface.onCompletePAyment();
                        res[0] = 1;


                    } else {

                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, status.getJSONObject("errNode").getString("errMsg"));
                        res[0] = 1;


                    }
                } catch (JSONException e) {
                    System.out.println(" JSON EXCEPTION " + e);

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                    res[0] = 1;


                }
            }

            @Override
            public void onErrorEncountered(String msg) {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);


            }
        }, ctx).performPostRequest(ctx, AllUtilUrls.base_url + "" + AllUtilUrls.place_order, map, "Please wait ...", 0);
        return res[0];
    }

    public static ArrayList<AllUtilSClass.TrackOrder> parseTrackOrderList(String result) {
        ArrayList<AllUtilSClass.TrackOrder> al_sub_category = new ArrayList<AllUtilSClass.TrackOrder>();
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONArray sub_category = pdata.getJSONArray("listItem");
            AllUtilSClass a = new AllUtilSClass();
            for (int i = 0; i < sub_category.length(); i++) {
                AllUtilSClass.TrackOrder cat = a.new TrackOrder();
                JSONObject cat_obj = sub_category.getJSONObject(i);
                cat.id = cat_obj.getString("id");
                cat.orderDate = cat_obj.getString("orderDate");
                cat.orderId = cat_obj.getString("orderId");
                cat.orderShip = cat_obj.getString("orderShip");
                cat.orderStatus = cat_obj.getString("orderStatus");
                cat.orderTotal = cat_obj.getString("orderTotal");
                al_sub_category.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return al_sub_category;
    }

    public static AllUtilSClass.MyOrderDetails parseMyOrderHistory(String result) {
        AllUtilSClass a = new AllUtilSClass();
        AllUtilSClass.MyOrderDetails od = a.new MyOrderDetails();
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONObject listItem = pdata.getJSONObject("listItem");
            od.gtotal = listItem.getString("gtotal");
            od.orderDate = listItem.getString("orderDate");
            if (listItem.getString("orderShipComp").contains("null")) {
                od.orderShipComp = "NA";
            } else {
                od.orderShipComp = listItem.getString("orderShipComp");
            }

            od.orderShipCty = listItem.getString("orderShipCty");
            od.orderShipFirstNm = listItem.getString("orderShipFirstNm");
            od.orderShipLastNm = listItem.getString("orderShipLastNm");
            od.orderShipPostCode = listItem.getString("orderShipPostCode");
            od.orderShipTel = listItem.getString("orderShipTel");
            od.orderShipStrt = listItem.getString("orderShipStrt");
            od.shipping = listItem.getString("shipping");
            od.stotal = listItem.getString("stotal");
            od.dtotal = listItem.getString("dtotal");

            JSONArray p_list = listItem.getJSONArray("productDetl");
            for (int i = 0; i < p_list.length(); i++) {
                AllUtilSClass.OrderDetailsList cat = a.new OrderDetailsList();
                JSONObject cat_obj = p_list.getJSONObject(i);
                cat.price = cat_obj.getString("price");
                cat.productId = cat_obj.getString("productId");
                cat.image_url = cat_obj.getString("image_url");
                cat.productName = cat_obj.getString("productName");
                cat.qty = cat_obj.getString("qty");
                cat.sku = cat_obj.getString("sku");
                cat.totalPrice = cat_obj.getString("totalPrice");
                od.al_order_items.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return od;
    }

    public static int addCouponCode(final Activity ctx, String shoppingcartid, String coupon_code, final CoordinatorLayout overview_coordinator_layout) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("coupon_code", coupon_code);
        map.put("shoppingcartid", shoppingcartid);
        final int[] res = {0};

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");

                    if (pdata.getString("success").equalsIgnoreCase("true")) {

                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("listItem"));
                        MyCartActivity ma = (MyCartActivity) ctx;
                        ma.addCouponUpdate();
                        res[0] = 1;


                    } else {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, status.getJSONObject("errNode").getString("errMsg"));


                    }
                } catch (JSONException e) {

                    System.out.println(" JSON EXCEPTION " + e);
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");


                }
            }

            @Override
            public void onErrorEncountered(String msg) {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);


            }
        }, ctx).performPostRequest(ctx, AllUtilUrls.base_url + "" + AllUtilUrls.coupon_code, map, "Please wait ...", 0);
        return res[0];
    }

    public static void setRVAnimation(RecyclerView rv) {
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(3000);
        itemAnimator.setRemoveDuration(3000);
        rv.setItemAnimator(itemAnimator);
    }

    public static boolean containsDigit(String s) {
        for (char c : s.toCharArray()) {
            if (Character.isDigit(c)) {
                return true;
            }
        }
        return false;
    }

    public static void shareBitmap(Activity ctx, Bitmap bitmap, String fileName,String product_name) {
        try {
            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator, fileName + ".jpg");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.putExtra(Intent.EXTRA_TEXT, product_name);
            intent.setType("*/*");
            ctx.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface AddCouponUpdate {
        public void addCouponUpdate();
    }

    public interface OnCartItemUpdate {
        public void onCartItemUpdate();
    }

    public static boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((GridLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)

                return true;
        }
        return false;
    }

    public static ArrayList<AllUtilSClass.Brand> parseBrandList(String result, Activity ctx) {
        ArrayList<AllUtilSClass.Brand> al_sub_category = new ArrayList<AllUtilSClass.Brand>();
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONArray sub_category = pdata.getJSONArray("listItem");

            AllUtilSClass a = new AllUtilSClass();
            for (int i = 0; i < sub_category.length(); i++) {
                AllUtilSClass.Brand cat = a.new Brand();
                JSONObject cat_obj = sub_category.getJSONObject(i);
                cat.id = cat_obj.getString("id");
                cat.value = cat_obj.getString("value");
                al_sub_category.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return al_sub_category;
    }

    public static ArrayList<AllUtilSClass.Price> parsePriceList(String result,Activity ctx) {
        ArrayList<AllUtilSClass.Price> al_sub_category = new ArrayList<AllUtilSClass.Price>();
        try {
            Log.e(" JSON ", result);
            JSONObject status = new JSONObject(result);
            JSONObject pdata = status.getJSONObject("pdata");
            JSONArray sub_category = pdata.getJSONArray("pricelist");


            AllUtilSClass a = new AllUtilSClass();
            for (int i = 0; i < sub_category.length(); i++) {
                AllUtilSClass.Price cat = a.new Price();
                JSONObject cat_obj = sub_category.getJSONObject(i);
                cat.min = cat_obj.getString("min");
                cat.max = cat_obj.getString("max");
                cat.value = "Rs. "+cat.min+" - Rs. "+cat.max;
                al_sub_category.add(cat);
            }
        } catch (JSONException e) {
            System.out.println(" JSON EXCEPTION " + e);
        }
        return al_sub_category;
    }


    public interface AccountDetailsRetrieved {
        public void accountDetailsRetrieved();
        public void accountDetailsRetrievedError();
    }

    public static int myAccount(final Activity ctx, String customer_id, final CoordinatorLayout overview_coordinator_layout, final AccountDetailsRetrieved mLIstener) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", customer_id);

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");

                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        JSONObject list_item = pdata.getJSONObject("listItem");
                        ctx.getSharedPreferences("login", 0).edit().putString("log", "yes").commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("temail", list_item.getString("email")).commit();
                        // ctx. getSharedPreferences("login", 0).edit().putString("customer_id", list_item.getString("customer_id")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("firstname", list_item.getString("firstname")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("lastname", list_item.getString("lastname")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("group_id", list_item.getString("group_id")).commit();

                        ctx.getSharedPreferences("login", 0).edit().putString("city", list_item.getString("city")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("street_address", list_item.getString("street_address")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("postcode", list_item.getString("postcode")).commit();
                        ctx.getSharedPreferences("login", 0).edit().putString("telephone", list_item.getString("telephone")).commit();
                        mLIstener.accountDetailsRetrieved();
                    } else {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("errMsg"));
                        ctx.getSharedPreferences("login", 0).edit().putString("log", "no").commit();
                    }
                } catch (JSONException e) {
                    System.out.println(" JSON EXCEPTION " + e);
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                    ctx.getSharedPreferences("login", 0).edit().putString("log", "no").commit();
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
                ctx.getSharedPreferences("login", 0).edit().putString("log", "no").commit();
            }
        }, ctx).performPostRequest(ctx, AllUtilUrls.base_url + "" + AllUtilUrls.myaccount, map, "Please wait ...", 0);
        return 1;
    }

    public static void changePasswordDialog(final Activity ctx, final CoordinatorLayout overview_coordinator_layout) {
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.change_password);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();

        final int[] return_val = {1};

        //final EditText edt_old_password=(EditText)dialog.findViewById(R.id.edt_old_password);
        final EditText edt_new_password = (EditText) dialog.findViewById(R.id.edt_new_password);
        final EditText edt_Confirm_password = (EditText) dialog.findViewById(R.id.edt_Confirm_password);

        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();

            }
        });

        Button btn_change = (Button) dialog.findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(ctx);
//                if (edt_old_password.getText().toString().trim().length() > 2) {
//                    return_val[0] =1;
//                } else {
//                    try {
//                        return_val[0] =0;
//                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter old password.");
//                    } catch (Exception exp) {
//                        AllUtilMethods.problemOccurDialog(ctx);
//                    }
//
//                }
                if (edt_new_password.getText().toString().trim().length() > 2) {
                    return_val[0] = 1;
                } else {
                    return_val[0] = 0;
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter new password.");
                }
                if (edt_Confirm_password.getText().toString().trim().length() > 2) {
                    return_val[0] = 1;
                } else {

                    return_val[0] = 0;
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter confirm password.");
                }
                if (edt_Confirm_password.getText().toString().trim().equalsIgnoreCase(edt_new_password.getText().toString().trim())) {
                    return_val[0] = 1;
                } else {
                    return_val[0] = 0;
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "New password & confirm password mismatch.");
                }
                if (return_val[0] == 1) {
                    dialog.cancel();
                    changePasswordWebCall(ctx
                            , ctx.getSharedPreferences("login", 0).getString("customer_id", "")
                            , edt_new_password.getText().toString().trim()
                            , overview_coordinator_layout);
                }
            }
        });
    }

    public static int changePasswordWebCall(final Activity ctx, String customer_id, String new_password, final CoordinatorLayout overview_coordinator_layout) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", customer_id);
        map.put("newpassword", new_password);

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("listItem"));
                    } else {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("errMsg"));
                    }
                } catch (JSONException e) {
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
            }
        }, ctx).performPostRequest(ctx, AllUtilUrls.base_url + "" + AllUtilUrls.change_psw, map, "Please wait ...", 0);
        return 1;
    }


    public static void forgotPasswordDialog(final Activity ctx, final CoordinatorLayout overview_coordinator_layout) {
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.forgot_password);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();

        final int[] return_val = {1};

        final EditText edt_email_id = (EditText) dialog.findViewById(R.id.edt_email_id);

        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        Button btn_send = (Button) dialog.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(ctx);
                if (edt_email_id.getText().toString().trim().length() > 0) {
                    return_val[0] = 1;
                } else {
                    return_val[0] = 0;
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Please enter your email id.");

                }
                if (isEmailValid(edt_email_id.getText().toString().trim())) {
                    return_val[0] = 1;
                } else {
                    return_val[0] = 0;
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Please enter valid email id.");
                }

                if (return_val[0] == 1) {
                    dialog.cancel();

                    forgotPasswordWebCall(ctx
                            , edt_email_id.getText().toString().trim()
                            , overview_coordinator_layout);
                }
            }
        });
    }


    public static int forgotPasswordWebCall(final Activity ctx, String new_password, final CoordinatorLayout overview_coordinator_layout) {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", new_password);

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("listItem"));
                    } else {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("errMsg"));
                    }
                } catch (JSONException e) {
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                }
            }

            @Override
            public void onErrorEncountered(String msg) {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);

            }
        }, ctx).performPostRequest(ctx, AllUtilUrls.base_url + "" + AllUtilUrls.forgetpassword, map, "Please wait ...", 0);
        return 1;
    }

    public static String getPercentage(double sale_price,double mrp) {
        double percentage = (mrp-sale_price)/mrp;
        percentage=percentage*100;
        DecimalFormat precision = new DecimalFormat("0.00");
        String str_percentage=precision.format(percentage);
        return str_percentage;
    }


    public interface MyCartGetSmsCallback {
        public void whenSmsreceived(String password);
    }
}

