package com.app.ingreens.inbx;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilAdapters.TabMenuOffersAdapter;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Papun's on 7/23/2016.
 */
public class AllCategories extends MyCustomActivity {
    //////////////////// UI VARIABLES ///////////////////
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;
    private CoordinatorLayout overview_coordinator_layout;
    private TextView toolbar_title;
    private RecyclerView rv;
    private ImageView imv_cart;
    private TextView tv_cart_count;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;
    ArrayList<AllUtilSClass.Category> al_category;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(AllCategories.this));
        setContentView(R.layout.home_supportive_activtiy);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);

        ImageView imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(AllCategories.this, NonHomePage.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        imv_cart=(ImageView)toolbar.findViewById(R.id.imv_cart);
        imv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Integer.parseInt(getSharedPreferences("login", 0).getString("cart_count","0"))>0){
                    Intent intent = new Intent(new Intent(AllCategories.this, MyCartActivity.class));
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
                }
                else{
                    AllUtilMethods.emptyCartDialog(AllCategories.this);
                }
            }
        });

        tv_cart_count=(TextView) toolbar.findViewById(R.id.tv_global_cart_count);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
                overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
                getCategories();

        }
        resume = true;
        tv_cart_count.setText(""+getSharedPreferences("login", 0).getString("cart_count","0"));
    }

    private void setUI() {

        System.out.println("   LOGGED IN DETAILS   " + getSharedPreferences("login", 0).getString("email", "")
                + "  " + getSharedPreferences("login", 0).getString("customer_id", "")
                + " " + getSharedPreferences("login", 0).getString("firstname", "")
                + "  " + getSharedPreferences("login", 0).getString("lastname", ""));

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tv_cart_count.setText(""+getSharedPreferences("login", 0).getString("cart_count","0"));

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
            }
        });
        setToolbarTitle("All Categories");

        rv = (RecyclerView) findViewById(R.id.rv);
        TabMenuOffersAdapter ama = new TabMenuOffersAdapter(AllCategories.this, al_category);

        GridLayoutManager lLayout = new GridLayoutManager(AllCategories.this, 2);

        rv.setHasFixedSize(true);
        rv.setLayoutManager(lLayout);
        AllUtilSClass a = new AllUtilSClass();
        rv.addItemDecoration(a.new NoSpacingItemDecoreation());
        rv.setAdapter(ama);
        AllUtilMethods.setRVAnimation(rv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Intent intent;
        AllUtilMethods.disableMenuItemForSometimes(item);
        switch (item.getItemId()) {
            case R.id.action_home:
                intent = new Intent(new Intent(AllCategories.this, NonHomePage.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_search:
                intent = new Intent(new Intent(AllCategories.this, SearchActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
///            case R.id.action_cart:
//                intent = new Intent(new Intent(AllCategories.this, MyCartActivity.class));
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_up, R.anim.stay);
 //               break;
            case R.id.action_account:
                intent = new Intent(new Intent(AllCategories.this, MyAccount.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);;
                break;
            case R.id.action_contact_us:
                intent = new Intent(new Intent(AllCategories.this, WebViewActivity.class));
                intent.putExtra("page","con");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_about_us:
                intent = new Intent(new Intent(AllCategories.this, WebViewActivity.class));
                intent.putExtra("page","ab");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_logout:
                intent = new Intent(new Intent(AllCategories.this, NonLoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                getSharedPreferences("login", 0).edit().clear().commit();
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_order:
                intent = new Intent(new Intent(AllCategories.this, TrackYourOrderActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_change_password:
                    AllUtilMethods.changePasswordDialog(AllCategories.this,overview_coordinator_layout);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }


    public void getCategories() {

        HashMap<String, String> map = new HashMap<String, String>();

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    Log.e(" JSON ", result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        al_category = AllUtilMethods.parseCategories(result);
                        if (al_category.size() > 0) {
                            setUI();
                        } else {
                            System.out.println(" Array BLANK ");
                            finish();
                        }
                    } else {
                            System.out.println(" ERROR " + pdata.getString("errMsg"));
                            AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("errMsg"));
                            finish();
                    }
                } catch (JSONException e) {
                        System.out.println(" JSON EXCEPTION " + e);
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                        finish();
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                    System.out.println(" SLIDE LEFT " + msg);
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
                    finish();
            }
        }, AllCategories.this).performPostRequest(AllCategories.this, AllUtilUrls.base_url + "" + AllUtilUrls.category, map, "Please wait ...", 0);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "AllCategories Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.app.ingreens.inbx/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "AllCategories Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.app.ingreens.inbx/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
    /////////////////////// THIS BELOW SEGMENT IS ONLY FOR FRAGMENT HANDLING ????????????????????????????????????????
}
