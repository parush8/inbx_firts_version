package com.app.ingreens.inbx;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilAdapters.TabMenuMostlyViewedAdapter;
import com.app.ingreens.inbx.UtilAdapters.TrackYourOrderAdapter;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Papun's on 7/23/2016.
 */
public class TrackYourOrderActivity extends MyCustomActivity {
    //////////////////// UI VARIABLES ///////////////////
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;
    CoordinatorLayout overview_coordinator_layout;
    private TextView toolbar_title;
    private RecyclerView rv;
    private ImageView imv_cart;
    private TextView tv_cart_count;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;
    private ArrayList<AllUtilSClass.TrackOrder> al_track_order;

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(TrackYourOrderActivity.this));
        setContentView(R.layout.track_your_order);

        overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);

        imv_cart = (ImageView) toolbar.findViewById(R.id.imv_cart);
        imv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(getSharedPreferences("login", 0).getString("cart_count", "0")) > 0) {
                    Intent intent = new Intent(new Intent(TrackYourOrderActivity.this, MyCartActivity.class));
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
                }
            }
        });

        tv_cart_count = (TextView) toolbar.findViewById(R.id.tv_global_cart_count);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            getOrderList();
        }
        resume = true;
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }

    private void setUI() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));

        ImageView imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(TrackYourOrderActivity.this, NonHomePage.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(new Intent(TrackYourOrderActivity.this, NonHomePage.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                finish();
            }
        });
        setToolbarTitle("MY Orders");


        rv = (RecyclerView) findViewById(R.id.rv);
        TrackYourOrderAdapter ama = new TrackYourOrderAdapter(TrackYourOrderActivity.this, al_track_order);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(TrackYourOrderActivity.this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(horizontalLayoutManagaer);
        rv.setAdapter(ama);
        AllUtilMethods.setRVAnimation(rv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //  getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        AllUtilMethods.disableMenuItemForSometimes(item);
        switch (item.getItemId()) {
            case R.id.action_home:
                intent = new Intent(new Intent(TrackYourOrderActivity.this, NonHomePage.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                finish();
                break;
            case R.id.action_search:
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Search");

                break;
            //   case R.id.action_cart:
//                intent = new Intent(new Intent(TrackYourOrderActivity.this, NonHomePage.class));
//                startActivity(intent);
//                overridePendingTransition( R.anim.slide_up, R.anim.stay );
            //  break;
            case R.id.action_account:
                intent = new Intent(new Intent(TrackYourOrderActivity.this, MyAccount.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_contact_us:
                intent = new Intent(new Intent(TrackYourOrderActivity.this, WebViewActivity.class));
                intent.putExtra("page", "con");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_about_us:
                intent = new Intent(new Intent(TrackYourOrderActivity.this, WebViewActivity.class));
                intent.putExtra("page", "ab");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_logout:
                intent = new Intent(new Intent(TrackYourOrderActivity.this, NonLoginActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_order:
                intent = new Intent(new Intent(TrackYourOrderActivity.this, TrackYourOrderActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_change_password:
                AllUtilMethods.changePasswordDialog(TrackYourOrderActivity.this, overview_coordinator_layout);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(new Intent(TrackYourOrderActivity.this, NonHomePage.class));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_up, R.anim.stay);
        finish();
    }

    public void getOrderList() {

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", getSharedPreferences("login", 0).getString("customer_id", "0"));

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                try {
                    System.out.println(" JSON jskdjfkasf " + result);
                    JSONObject status = new JSONObject(result);
                    JSONObject pdata = status.getJSONObject("pdata");
                    if (pdata.getString("success").equalsIgnoreCase("true")) {
                        al_track_order = AllUtilMethods.parseTrackOrderList(result);
                        if (al_track_order.size() > 0) {
                            setUI();
                        }
                    } else {
                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "No Order list found.");
                    }
                } catch (JSONException e) {
                    System.out.println(" JSON EXCEPTION " + e);
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
                    finish();
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);

            }
        }, TrackYourOrderActivity.this).performPostRequest(TrackYourOrderActivity.this, AllUtilUrls.base_url +""+ AllUtilUrls.orderlist, map, "Please wait ...", 0);
    }

    /////////////////////// THIS BELOW SEGMENT IS ONLY FOR FRAGMENT HANDLING ????????????????????????????????????????
}
