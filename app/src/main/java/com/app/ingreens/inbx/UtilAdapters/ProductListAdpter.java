package com.app.ingreens.inbx.UtilAdapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.ProductDetailsActivity;
import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;

import java.util.List;

/**
 * Created by Papun's on 7/24/2016.
 */
public class ProductListAdpter extends RecyclerView.Adapter<ProductListAdpter.MyViewHolder> {

    private List<AllUtilSClass.ProductList> moviesList;
    private Activity ctx;
    private CoordinatorLayout overview_coordinator_layout;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_price, tv_rgprice, tv_tier_prices,tv_percentage;
        public ImageView imv_category;
        public Button btn_add_to_cart;

        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_price = (TextView) view.findViewById(R.id.tv_price);
            tv_rgprice = (TextView) view.findViewById(R.id.tv_rgprice);
            imv_category = (ImageView) view.findViewById(R.id.imv_product_image);
            tv_tier_prices = (TextView) view.findViewById(R.id.tv_tier_prices);
            btn_add_to_cart = (Button) view.findViewById(R.id.btn_add_to_cart);
            tv_percentage= (TextView) view.findViewById(R.id.tv_percentage);
        }
    }

    public ProductListAdpter(RecyclerView mRecyclerView, Activity ctx, List<AllUtilSClass.ProductList> moviesList, CoordinatorLayout overview_coordinator_layout) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.overview_coordinator_layout = overview_coordinator_layout;

    }

    @Override
    public ProductListAdpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(ctx).inflate(R.layout.product_list_cell, parent, false);
            return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ProductListAdpter.MyViewHolder holder, final int position) {
            MyViewHolder customholder = (MyViewHolder) holder;
            customholder.tv_title.setText(moviesList.get(position).name);
            customholder.tv_title.setSelected(true);

            if (moviesList.get(position).stock_status.equalsIgnoreCase("0")) {
                customholder.btn_add_to_cart.setText("Out of Stock");
                customholder.btn_add_to_cart.setEnabled(false);
            } else {
                customholder.btn_add_to_cart.setText("Add to Cart");
                customholder.btn_add_to_cart.setEnabled(true);
            }

            customholder.btn_add_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println("   LOGGED IN DETAILS PRODUCT LIST  " + ctx.getSharedPreferences("login", 0).getString("email", "")
                            + "  " + ctx.getSharedPreferences("login", 0).getString("customer_id", "")
                            + " " + ctx.getSharedPreferences("login", 0).getString("firstname", "")
                            + "  " + ctx.getSharedPreferences("login", 0).getString("lastname", ""));
                    AllUtilMethods.addToCart(ctx,
                            ctx.getSharedPreferences("login", 0).getString("shoppingcartid", "")
                            , ctx.getSharedPreferences("login", 0).getString("customer_id", "")
                            , moviesList.get(position).product_id, "1",
                            overview_coordinator_layout, 0);

                }
            });

            if (moviesList.get(position).spprice.length() > 3) {
                customholder.tv_price.setText("" + moviesList.get(position).spprice);
                customholder.tv_rgprice.setVisibility(View.VISIBLE);
                customholder.tv_rgprice.setText("M.R.P. " + moviesList.get(position).rgprice);
                customholder.tv_rgprice.setPaintFlags(customholder.tv_rgprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                customholder.tv_percentage.setVisibility(View.VISIBLE);

                String sale_price=moviesList.get(position).spprice;
                sale_price=sale_price.replace("Rs. ","").replace(",","").trim();

                String mrp_price=moviesList.get(position).rgprice;
                mrp_price=mrp_price.replace("Rs. ","").replace(",","").trim();

                customholder.tv_percentage.setText(""+AllUtilMethods.getPercentage(Double.parseDouble(sale_price),Double.parseDouble(mrp_price))+"% Off.");
            } else {
                customholder.tv_price.setText("M.R.P. " + moviesList.get(position).rgprice);
                customholder.tv_rgprice.setVisibility(View.INVISIBLE);
                customholder.tv_percentage.setVisibility(View.INVISIBLE);
            }

            AllUtilMethods.getImage(ctx, R.drawable.subcategory_placeholder, moviesList.get(position).image_url, customholder.imv_category, 300, 300);

            customholder.imv_category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(new Intent(ctx, ProductDetailsActivity.class));
                    intent.putExtra("cat_id", moviesList.get(position).product_id);
                    ctx.startActivity(intent);
                    ctx.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                }
            });

            if (moviesList.get(position).tierPrices.length() > 2) {
                customholder.tv_tier_prices.setText(""+moviesList.get(position).tierPrices);
                customholder.tv_tier_prices.setVisibility(View.VISIBLE);
            } else {
                customholder.tv_tier_prices.setVisibility(View.INVISIBLE);
            }
    }

    @Override
    public int getItemCount() {
        return moviesList == null ? 0 : moviesList.size();
    }
}
