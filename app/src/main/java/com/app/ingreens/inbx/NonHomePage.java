package com.app.ingreens.inbx;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.View;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.framents.HomeFragment;
import com.app.ingreens.inbx.framents.SlideCategoryLeftPanel;

public class NonHomePage extends MyCustomActivity implements
        View.OnClickListener {

    //////////////////// UI VARIABLES ///////////////////
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    FrameLayout fm_left_slide_menu, fm_container;
    public CoordinatorLayout overview_coordinator_layout;
    private TextView toolbar_title;
    private ImageView imv_home_logo;
    private ImageView imv_cart;
    private TextView tv_cart_count;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;
    int drawer_flag = 0;

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(NonHomePage.this));

        setContentView(R.layout.non_home_page);

        overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setupDrawer();
        drawerLayout.setDrawerListener(mDrawerToggle);
        imv_home_logo.setOnClickListener(NonHomePage.this);

        imv_cart = (ImageView) toolbar.findViewById(R.id.imv_cart);
        imv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(getSharedPreferences("login", 0).getString("cart_count", "0")) > 0) {
                    //stopTimer(0);
                    Intent intent = new Intent(new Intent(NonHomePage.this, MyCartActivity.class));
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
                } else {
                    AllUtilMethods.emptyCartDialog(NonHomePage.this);
                }
            }
        });

        tv_cart_count = (TextView) toolbar.findViewById(R.id.tv_global_cart_count);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            setUI();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fm_left_slide_menu, new SlideCategoryLeftPanel(), "SlideCategoryLeftPanel")
                    .addToBackStack("SlideCategoryLeftPanel")
                    .commit();

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fm_container, new HomeFragment(), "HomeFragment")
                    .addToBackStack("HomeFragment")
                    .commit();

        }
        resume = true;
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }

    private void setUI() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));

        fm_left_slide_menu = (FrameLayout) findViewById(R.id.fm_left_slide_menu);
        fm_container = (FrameLayout) findViewById(R.id.fm_container);

        setToolbarTitle("Home Page");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        AllUtilMethods.disableMenuItemForSometimes(item);
        switch (item.getItemId()) {
            case R.id.action_home:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fm_container, new HomeFragment(), "HomeFragment")
                        .addToBackStack("HomeFragment")
                        .commit();

                setToolbarTitle("Home Page");
                break;
            case R.id.action_search:
                // stopTimer(0);
                intent = new Intent(new Intent(NonHomePage.this, SearchActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            //      case R.id.action_cart:
//                stopTimer(0);
//               intent = new Intent(new Intent(NonHomePage.this, MyCartActivity.class));
//                startActivity(intent);
//                overridePendingTransition( R.anim.slide_up, R.anim.stay );
            //           break;
            case R.id.action_account:
                //  stopTimer(0);
                intent = new Intent(new Intent(NonHomePage.this, MyAccount.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_contact_us:
                // stopTimer(0);
                intent = new Intent(new Intent(NonHomePage.this, WebViewActivity.class));
                intent.putExtra("page", "con");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_about_us:
                //  stopTimer(0);
                intent = new Intent(new Intent(NonHomePage.this, WebViewActivity.class));
                intent.putExtra("page", "ab");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_logout:
                // stopTimer(1);
                intent = new Intent(new Intent(NonHomePage.this, NonLoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                getSharedPreferences("login", 0).edit().clear().commit();
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_order:
                //  stopTimer(0);
                intent = new Intent(new Intent(NonHomePage.this, TrackYourOrderActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_change_password:
                AllUtilMethods.changePasswordDialog(NonHomePage.this, overview_coordinator_layout);
                break;
        }

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                mDrawerToggle.syncState();
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                mDrawerToggle.syncState();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        toolbar.setNavigationIcon(R.drawable.menu_list);
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer_flag == 0) {
                    drawer_flag = 1;
                    drawerLayout.openDrawer(Gravity.RIGHT);
                } else {
                    drawer_flag = 0;
                    drawerLayout.closeDrawer(Gravity.LEFT);
                }
            }
        });
        drawerLayout.setDrawerListener(mDrawerToggle);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_home_logo:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fm_container, new HomeFragment(), "HomeFragment")
                        .addToBackStack("HomeFragment")
                        .commit();
                setToolbarTitle("Home Page");
                break;
        }
    }

    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
    }

    @Override
    public void onBackPressed() {
        AllUtilMethods.appCloseDialog("Are you sure want to close InBx?", NonHomePage.this);
    }

    //    public void stopTimer(int finish){
//        HomeFragment fragment = (HomeFragment)getSupportFragmentManager().findFragmentByTag("HomeFragment");
//
//        if(fragment!=null){
//            fragment.swipeTimer.cancel();
//        }
//        if(finish==1){
//            try{
//                finish();
//            } catch(Exception e){
//                System.out.println(" FRAGMENT EXCEPTION "+e);
//            }
//        }
//    }
    /////////////////////// THIS BELOW SEGMENT IS ONLY FOR FRAGMENT HANDLING ????????????????????????????????????????
    @Override
    public void onCartItemUpdate() {
        super.onCartItemUpdate();
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }
}
