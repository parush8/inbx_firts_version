package com.app.ingreens.inbx;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;

import java.util.ArrayList;

/**
 * Created by Papun's on 7/23/2016.
 */
public class ChangeAddess extends MyCustomActivity implements View.OnClickListener, AllUtilMethods.OnPaymentCompleteCallback, AllUtilMethods.AccountDetailsRetrieved {
    //////////////////// UI VARIABLES ///////////////////
    Button btn_proceed_to_check_out;
    private Toolbar toolbar;
    private CoordinatorLayout overview_coordinator_layout;
    private TextView toolbar_title;
    private EditText edt_fname, edt_lname, edt_email, edt_add_1, edt_add_2, edt_pin, edt_city, edt_state, edt_company, edt_tele;
    private EditText edt_sfname, edt_slname, edt_semail, edt_sadd_1, edt_sadd_2, edt_spin, edt_scity, edt_sstate, edt_scompany, edt_stele;
    private LinearLayout ll_ship_address, ll_bill_address;
    private RadioGroup rg_address_mode;
    private RadioButton rb_same_shipping_address, rb_different_shipping_address;
    private ScrollView scv_add;
    private ImageView imv_cart;
    private TextView tv_cart_count;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;
    private int shipping_mode = 0;

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(ChangeAddess.this));
        setContentView(R.layout.change_address);

        overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);

        imv_cart = (ImageView) toolbar.findViewById(R.id.imv_cart);
        imv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(getSharedPreferences("login", 0).getString("cart_count", "0")) > 0) {
                    Intent intent = new Intent(new Intent(ChangeAddess.this, MyCartActivity.class));
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
                } else {
                    AllUtilMethods.emptyCartDialog(ChangeAddess.this);
                }

            }
        });

        tv_cart_count = (TextView) toolbar.findViewById(R.id.tv_global_cart_count);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
                 AllUtilMethods.myAccount(ChangeAddess.this, getSharedPreferences("login", 0).getString("customer_id", ""), overview_coordinator_layout,ChangeAddess.this);

        }
        resume = true;
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }

    private void setUI() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tv_cart_count.setText(""+getSharedPreferences("login", 0).getString("cart_count","0"));

        ImageView imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(ChangeAddess.this, NonHomePage.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
            }
        });
        setToolbarTitle("Your Address");


//        getSharedPreferences("login", 0).getString("log", "")
//        getSharedPreferences("login", 0).getString("email", "")
//        getSharedPreferences("login", 0).getString("customer_id", "")
//        getSharedPreferences("login", 0).getString("firstname", "")
//        getSharedPreferences("login", 0).getString("lastname", "")
//        getSharedPreferences("login", 0).getString("group_id", "")
//
//        getSharedPreferences("login", 0).getString("city","")
//        getSharedPreferences("login", 0).getString("street_address", "")
//        getSharedPreferences("login", 0).getString("postcode", "")
//        getSharedPreferences("login", 0).getString("telephone","")

        scv_add = (ScrollView) findViewById(R.id.scv_add);

        edt_fname = (EditText) findViewById(R.id.edt_fname);
        edt_fname.setText(getSharedPreferences("login", 0).getString("firstname", ""));

        edt_lname = (EditText) findViewById(R.id.edt_lname);
        edt_lname.setText(getSharedPreferences("login", 0).getString("lastname", ""));

        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_email.setText("" + getSharedPreferences("login", 0).getString("email", ""));

        edt_add_1 = (EditText) findViewById(R.id.edt_add_1);
        edt_add_1.setText("" + getSharedPreferences("login", 0).getString("street_address", ""));

        edt_add_2 = (EditText) findViewById(R.id.edt_add_2);

        edt_pin = (EditText) findViewById(R.id.edt_pin);
        edt_pin.setText("" + getSharedPreferences("login", 0).getString("postcode", ""));

        edt_city = (EditText) findViewById(R.id.edt_city);
        edt_city.setText("" + getSharedPreferences("login", 0).getString("city", ""));

        edt_state = (EditText) findViewById(R.id.edt_state);
        edt_state.setEnabled(false);

        edt_company = (EditText) findViewById(R.id.edt_company);

        edt_tele = (EditText) findViewById(R.id.edt_tele);
        edt_tele.setText("" + getSharedPreferences("login", 0).getString("telephone", ""));

        ll_ship_address = (LinearLayout) findViewById(R.id.ll_ship_address);
        ll_bill_address = (LinearLayout) findViewById(R.id.ll_bill_address);
        rg_address_mode = (RadioGroup) findViewById(R.id.rg_address_mode);


        rb_same_shipping_address = (RadioButton) findViewById(R.id.rb_same_shipping_address);
        rb_same_shipping_address.setChecked(true);

        rb_different_shipping_address = (RadioButton) findViewById(R.id.rb_different_shipping_address);

        edt_sfname = (EditText) findViewById(R.id.edt_sfname);

        edt_slname = (EditText) findViewById(R.id.edt_slname);

        edt_semail = (EditText) findViewById(R.id.edt_smail);

        edt_sadd_1 = (EditText) findViewById(R.id.edt_sadd_1);

        edt_sadd_2 = (EditText) findViewById(R.id.edt_sadd_2);

        edt_spin = (EditText) findViewById(R.id.edt_spin);

        edt_scity = (EditText) findViewById(R.id.edt_scity);

        edt_sstate = (EditText) findViewById(R.id.edt_sstate);
        edt_sstate.setEnabled(false);

        edt_scompany = (EditText) findViewById(R.id.edt_scompany);

        edt_stele = (EditText) findViewById(R.id.edt_stele);

        btn_proceed_to_check_out = (Button) findViewById(R.id.btn_proceed_to_check_out);
        btn_proceed_to_check_out.setOnClickListener(this);

        rg_address_mode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.rb_different_shipping_address) {
                    ll_ship_address.setVisibility(View.VISIBLE);
                    shipping_mode = 1;
                    scv_add.post(new Runnable() {
                        public void run() {
                            scv_add.scrollTo(0, scv_add.getBottom());
                        }
                    });
                } else if (checkedId == R.id.rb_same_shipping_address) {
                    ll_ship_address.setVisibility(View.GONE);
                    shipping_mode = 0;
                    scv_add.post(new Runnable() {
                        public void run() {
                            scv_add.scrollTo(0, scv_add.getTop());
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Intent intent;
        AllUtilMethods.disableMenuItemForSometimes(item);
        switch (item.getItemId()) {
            case R.id.action_home:
                intent = new Intent(new Intent(ChangeAddess.this, NonHomePage.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);

                break;
            case R.id.action_search:
                intent = new Intent(new Intent(ChangeAddess.this, SearchActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            //      case R.id.action_cart:
//                intent = new Intent(new Intent(ChangeAddess.this, MyCartActivity.class));
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            //            break;
            case R.id.action_account:
                intent = new Intent(new Intent(ChangeAddess.this, MyAccount.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_contact_us:
                intent = new Intent(new Intent(ChangeAddess.this, WebViewActivity.class));
                intent.putExtra("page", "con");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_about_us:
                intent = new Intent(new Intent(ChangeAddess.this, WebViewActivity.class));
                intent.putExtra("page", "ab");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_logout:
                intent = new Intent(new Intent(ChangeAddess.this, NonLoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                getSharedPreferences("login", 0).edit().clear().commit();
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_order:
                intent = new Intent(new Intent(ChangeAddess.this, TrackYourOrderActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_change_password:

                    AllUtilMethods.changePasswordDialog(ChangeAddess.this,overview_coordinator_layout);

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_proceed_to_check_out:
                showPaymentDialog(ChangeAddess.this);
                break;
        }
    }

    public void showPaymentDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.payment_mode_dialog);

        Button btn_online_paymeny = (Button) dialog.findViewById(R.id.btn_online_paymeny);
        btn_online_paymeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btn_cod = (Button) dialog.findViewById(R.id.btn_cod);
        btn_cod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                if (checkMissingField() == 1) {
                    String fname = "", lname = "", sfname = "", slname = "";
                    fname = edt_fname.getText().toString().trim();
                    lname = edt_lname.getText().toString().trim();
                    sfname = edt_sfname.getText().toString().trim();
                    slname = edt_slname.getText().toString().trim();
                    if (shipping_mode == 1) {
                        AllUtilMethods.placeOrder(
                                ChangeAddess.this,
                                getSharedPreferences("login", 0).getString("shoppingcartid", ""),
                                getSharedPreferences("login", 0).getString("customer_id", "0"),
                                "billing",
                                fname,
                                lname,
                                edt_email.getText().toString().trim(),
                                edt_add_1.getText().toString().trim(),
                                edt_city.getText().toString().trim(),
                                edt_pin.getText().toString().trim(),
                                edt_tele.getText().toString().trim(),
                                edt_company.getText().toString().trim(),
                                sfname,
                                slname,
                                edt_scompany.getText().toString().trim(),
                                edt_sadd_1.getText().toString().trim(),
                                edt_scity.getText().toString().trim(),
                                edt_spin.getText().toString().trim(),
                                edt_stele.getText().toString().trim(),

                                overview_coordinator_layout,
                                ChangeAddess.this
                        );
                    } else {
                        AllUtilMethods.placeOrder(
                                ChangeAddess.this,
                                getSharedPreferences("login", 0).getString("shoppingcartid", ""),
                                getSharedPreferences("login", 0).getString("customer_id", "0"),
                                "billing",
                                fname,
                                lname,
                                edt_email.getText().toString().trim(),
                                edt_add_1.getText().toString().trim(),
                                edt_city.getText().toString().trim(),
                                edt_pin.getText().toString().trim(),
                                edt_tele.getText().toString().trim(),
                                edt_company.getText().toString().trim(),
                                fname,
                                lname,
                                edt_company.getText().toString().trim(),
                                edt_add_1.getText().toString().trim(),
                                edt_city.getText().toString().trim(),
                                edt_pin.getText().toString().trim(),
                                edt_tele.getText().toString().trim(),

                                overview_coordinator_layout,
                                ChangeAddess.this
                        );
                    }
                }
            }
        });

        dialog.show();

    }

    public int checkMissingField() {


        if (edt_fname.getText().toString().trim().length() > 2) {

        } else {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter first name field.");


            return 0;
        }
        if (AllUtilMethods.containsDigit(edt_fname.getText().toString().trim())) {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "First name can not contains digit.");


            return 0;
        } else {

        }
        if (edt_lname.getText().toString().trim().length() > 2) {

        } else {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter last name field.");


            return 0;
        }
        if (AllUtilMethods.containsDigit(edt_lname.getText().toString().trim())) {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Last name can not contains digit.");


            return 0;
        } else {

        }

        if (edt_email.getText().toString().trim().length() > 2) {

        } else {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter email id field.");


            return 0;
        }
        if (AllUtilMethods.isEmailValid(edt_email.getText().toString().trim())) {

        } else {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter valid email id field.");


            return 0;
        }
        if (edt_add_1.getText().toString().trim().length() > 2) {

        } else {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter address1 field.");


            return 0;
        }
//        if (edt_add_2.getText().toString().trim().length() > 2) {
//
//        } else {
//            AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter address2 field.");
//            return 0;
//        }
        if (edt_pin.getText().toString().trim().length() > 2) {

        } else {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter pin field.");

            return 0;
        }
        if (edt_city.getText().toString().trim().length() > 2) {

        } else {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a city field.");


            return 0;
        }
        if (AllUtilMethods.containsDigit(edt_city.getText().toString().trim())) {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "City name can not contains digit.");


            return 0;
        } else {

        }
        if (edt_state.getText().toString().trim().length() > 2) {

        } else {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a state field.");


            return 0;
        }
        if (AllUtilMethods.containsDigit(edt_state.getText().toString().trim())) {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "State name can not contains digit.");


            return 0;
        } else {

        }
//        if (edt_company.getText().toString().trim().length() > 2) {
//
//        } else {
//            AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a company field.");
//            return 0;
//        }
        if (AllUtilMethods.containsDigit(edt_company.getText().toString().trim())) {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Company name can not contains digit.");


            return 0;
        } else {

        }
        if (edt_tele.getText().toString().trim().length() > 2) {

        } else {

                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a telephone number field.");

            return 0;
        }

        if (shipping_mode == 1) {
            if (edt_sfname.getText().toString().trim().length() > 2) {

            } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a shipping first name field.");


                return 0;
            }
            if (AllUtilMethods.containsDigit(edt_sfname.getText().toString().trim())) {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "First name can not contains digit.");


                return 0;
            } else {

            }
            if (edt_slname.getText().toString().trim().length() > 2) {

            } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a shipping last name field.");

                return 0;
            }
            if (AllUtilMethods.containsDigit(edt_slname.getText().toString().trim())) {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Last name can not contains digit.");


                return 0;
            } else {

            }
            if (edt_semail.getText().toString().trim().length() > 2) {

            } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a Shipping email field.");


                return 0;
            }
            if (AllUtilMethods.isEmailValid(edt_semail.getText().toString().trim())) {

            } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter valid email id field.");

                return 0;
            }
            if (edt_sadd_1.getText().toString().trim().length() > 2) {

            } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a Shipping address1 field.");

                return 0;
            }
//            if (edt_sadd_2.getText().toString().trim().length() > 2) {
//
//            } else {
//                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a Shipping address2 field.");
//                return 0;
//            }
            if (edt_spin.getText().toString().trim().length() > 2) {

            } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a Shipping pin field.");

                return 0;
            }
            if (edt_scity.getText().toString().trim().length() > 2) {

            } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a Shipping city field.");


                return 0;
            }
            if (AllUtilMethods.containsDigit(edt_scity.getText().toString().trim())) {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Shipping City name can not contains digit.");


                return 0;
            } else {

            }
            if (edt_sstate.getText().toString().trim().length() > 2) {

            } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a Shipping State field.");

                return 0;
            }
            if (AllUtilMethods.containsDigit(edt_sstate.getText().toString().trim())) {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Shipping State name can not contains digit.");


                return 0;
            } else {

            }
//            if (edt_scompany.getText().toString().trim().length() > 2) {
//
//            } else {
//                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a Shipping Company field.");
//                return 0;
//            }
            if (AllUtilMethods.containsDigit(edt_scompany.getText().toString().trim())) {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Shipping Company name can not contains digit.");



                return 0;
            } else {

            }
            if (edt_stele.getText().toString().trim().length() > 2) {

            } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Enter a Shipping Telephone number field.");


                return 0;
            }
        }
        return 1;
    }

    @Override
    public void onCompletePAyment() {
        getSharedPreferences("login", 0).edit().putString("shoppingcartid", "").commit();
        Intent intent = new Intent(new Intent(ChangeAddess.this, TrackYourOrderActivity.class));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_up, R.anim.stay);
        finish();
    }

    @Override
    public void accountDetailsRetrieved() {
        setUI();
    }

    @Override
    public void accountDetailsRetrievedError() {
        finish();
    }
}
