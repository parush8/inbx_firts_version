package com.app.ingreens.inbx;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by root on 7/6/16.
 */
public class NonLoginActivity extends MyCustomActivity implements View.OnClickListener,AllUtilMethods.MyCartGetSmsCallback {

    //////////////////// UI VARIABLES ///////////////////
    ImageView imb_login;
    EditText edt_email_id, edt_psw;
    TextView tv_forgot_password;
    CoordinatorLayout overview_coordinator_layout;
    ProgressDialog pd;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(NonLoginActivity.this));
        setContentView(R.layout.non_login);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            setUI();
        }
        resume = true;
    }

    private void setUI() {
        overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);

        final InBxApplication mApp = ((InBxApplication) NonLoginActivity.this.getApplicationContext());
        mApp.na = this;

        imb_login = (ImageView) findViewById(R.id.imb_login);
        imb_login.setOnClickListener(this);

        edt_email_id = (EditText) findViewById(R.id.edt_email_id);
        edt_email_id.setText("" + getSharedPreferences("login", 0).getString("email", ""));

        edt_psw = (EditText) findViewById(R.id.edt_psw);

        tv_forgot_password = (TextView) findViewById(R.id.tv_forgot_password);
        tv_forgot_password.setOnClickListener(this);
        tv_forgot_password.setPaintFlags(tv_forgot_password.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imb_login:

                if (edt_email_id.getText().toString().trim().length() > 0) {

                } else {

                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Please Enter Your Email ID.");
                    break;
                }
                if (edt_psw.getText().toString().trim().length() > 0) {

                } else {
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Please Enter Your Password.");
                    break;
                }
                if (AllUtilMethods.isEmailValid(edt_email_id.getText().toString().trim())) {

                } else {
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Please Enter Valid Email ID.");
                    break;
                }
                AllUtilMethods.hideKeyboard(NonLoginActivity.this);
                AllUtilMethods.login(NonLoginActivity.this,
                        edt_email_id.getText().toString().trim(),
                        edt_psw.getText().toString().trim(), overview_coordinator_layout, getSharedPreferences("user_data", 0).getString("reg_id", ""));
                break;
            case R.id.tv_forgot_password:
                AllUtilMethods.forgotPasswordDialog(NonLoginActivity.this, overview_coordinator_layout);
                break;
        }
    }

    @Override
    public void whenSmsreceived(String password) {
            edt_psw.setText(password.trim());
    }
}
