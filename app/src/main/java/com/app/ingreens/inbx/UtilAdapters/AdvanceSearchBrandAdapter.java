package com.app.ingreens.inbx.UtilAdapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.InBxApplication;
import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.SubCategory;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;

import java.util.List;

/**
 * Created by root on 8/19/16.
 */
public class AdvanceSearchBrandAdapter extends RecyclerView.Adapter<AdvanceSearchBrandAdapter.MyViewHolder> {

    private List<AllUtilSClass.Brand> moviesList;
    private Activity ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox chk_filter;

        public MyViewHolder(View view) {
            super(view);
            chk_filter = (CheckBox) view.findViewById(R.id.chk_filter);
        }
    }


    public AdvanceSearchBrandAdapter(Activity ctx, List<AllUtilSClass.Brand> moviesList) {
        this.moviesList = moviesList;
        this.ctx=ctx;
       // final InBxApplication mApp = ((InBxApplication)ctx.getApplicationContext());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.filter_list_cell, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,final int position) {
        final InBxApplication mApp = ((InBxApplication)ctx.getApplicationContext());
        holder.chk_filter.setText(moviesList.get(position).value);

        if(moviesList.get(position).check==1){
            holder.chk_filter.setChecked(true);
        }else{
            holder.chk_filter.setChecked(false);
        }

        holder.chk_filter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    moviesList.get(position).check=1;
                    mApp.al_brand_list.get(position).check=1;

                }else{
                    moviesList.get(position).check=0;
                    mApp.al_brand_list.get(position).check=0;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
