//package com.app.ingreens.inbx.framents;
//
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.v4.app.DialogFragment;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageButton;
//
//import com.app.ingreens.inbx.R;
//
///**
// * Created by root on 8/19/16.
// */
//public class AdvanceSearchFilterFragment extends DialogFragment implements View.OnClickListener {
//
//    ////////////// UI ////////////////////////////////////////////
//    private ImageButton btn_delete;
//    private Button btn_clear, btn_brand, btn_price;
//    private RecyclerView rv;
//    /////////////////////////////////////////////////////////////
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        View view = inflater.inflate(R.layout.advance_search_filter_activity, container);
//
//        return view;
//    }
//
//    public AdvanceSearchFilterFragment(){
//
//    }
//
//    private void setUI(View view){
//        btn_delete = (ImageButton)view. findViewById(R.id.btn_delete);
//        btn_delete.setOnClickListener(this);
//
//        btn_clear = (Button)view. findViewById(R.id.btn_clear);
//
//        btn_brand = (Button)view. findViewById(R.id.btn_brand);
//        btn_brand.setOnClickListener(this);
//
//        btn_price = (Button)view. findViewById(R.id.btn_price);
//        btn_price.setOnClickListener(this);
//
//        rv = (RecyclerView)view. findViewById(R.id.rv);
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.btn_delete:
//                dismiss();
//                break;
//            case R.id.btn_brand:
//                btn_brand.setBackgroundResource(R.drawable.brand);
//                btn_price.setBackgroundResource(R.drawable.brand_grey);
//                btn_brand.setTextColor(Color.WHITE);
//                btn_price.setTextColor(Color.BLACK);
//                break;
//            case R.id.btn_price:
//                btn_brand.setBackgroundResource(R.drawable.brand_grey);
//                btn_price.setBackgroundResource(R.drawable.brand);
//                btn_brand.setTextColor(Color.BLACK);
//                btn_price.setTextColor(Color.WHITE);
//                break;
//        }
//    }
//}
