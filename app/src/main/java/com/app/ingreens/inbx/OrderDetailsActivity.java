package com.app.ingreens.inbx;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Papun's on 8/12/2016.
 */
public class OrderDetailsActivity extends MyCustomActivity implements View.OnClickListener {
    //////////////////// UI VARIABLES ///////////////////
    private Toolbar toolbar;
    private CoordinatorLayout overview_coordinator_layout;
    private TextView toolbar_title, tv_name, tv_order_date, tv_address, tv_tele, tv_total, tv_shipping, tv_grand_total, tv_company, tv_discount;
    private LinearLayout ll_holder;
    private Button btn_cancel_order;
    private ImageView imv_cart;
    private TextView tv_cart_count;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;
    AllUtilSClass.MyOrderDetails my_order_details = null;
    private ArrayList<AllUtilSClass.OrderDetailsViewHolder> al_viewholder;

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(OrderDetailsActivity.this));
        setContentView(R.layout.order_details);

        overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);

        imv_cart = (ImageView) toolbar.findViewById(R.id.imv_cart);
        imv_cart.setVisibility(View.GONE);
//        imv_cart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(Integer.parseInt(getSharedPreferences("login", 0).getString("cart_count","0"))>0){
//                    Intent intent = new Intent(new Intent(OrderDetailsActivity.this, MyCartActivity.class));
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
//                }
//            }
//        });

        tv_cart_count = (TextView) toolbar.findViewById(R.id.tv_global_cart_count);
        tv_cart_count.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            String order_id = getIntent().getExtras().getString("order_id");
            System.out.println("  NOTIFICATION   "+order_id.toString());
            getOrderItemList(getSharedPreferences("login", 0).getString("customer_id", "0"), order_id);
        }
        resume = true;
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }

    private void setUI() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ImageView imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(OrderDetailsActivity.this, NonHomePage.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));

        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_name.setText("" + my_order_details.orderShipFirstNm + " " + my_order_details.orderShipLastNm);

        tv_order_date = (TextView) findViewById(R.id.tv_order_date);
        tv_order_date.setText("" + my_order_details.orderDate);

        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_address.setText("Address: " + my_order_details.orderShipStrt + "," + my_order_details.orderShipCty + "\n" + my_order_details.orderShipPostCode);

        tv_tele = (TextView) findViewById(R.id.tv_tele);
        tv_tele.setText("Telephone: " + my_order_details.orderShipTel);

        tv_total = (TextView) findViewById(R.id.tv_total);
        tv_total.setText("" + my_order_details.stotal);

        tv_shipping = (TextView) findViewById(R.id.tv_shipping);
        tv_shipping.setText("" + my_order_details.shipping);

        tv_grand_total = (TextView) findViewById(R.id.tv_grand_total);
        tv_grand_total.setText("" + my_order_details.gtotal);

        tv_company = (TextView) findViewById(R.id.tv_company);
        tv_company.setText("Company: " + my_order_details.orderShipComp);

        tv_discount = (TextView) findViewById(R.id.tv_discount);
        tv_discount.setText("" + my_order_details.dtotal);

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
            }
        });
        setToolbarTitle("Order Details");

        btn_cancel_order = (Button) findViewById(R.id.btn_cancel_order);
        btn_cancel_order.setOnClickListener(this);

        ll_holder = (LinearLayout) findViewById(R.id.ll_holder);
        loadMenuItem();

    }

    private void loadMenuItem() {
        ll_holder.removeAllViews();
        al_viewholder = new ArrayList<AllUtilSClass.OrderDetailsViewHolder>();
        for (int i = 0; i < my_order_details.al_order_items.size(); i++) {
            ll_holder.addView(addMenuView(i));
        }
        System.out.println(" TOTAL ");
    }


    public View addMenuView(final int position) {
        View v;
        v = LayoutInflater.from(OrderDetailsActivity.this).inflate(R.layout.order_details_list_cell, null);
        AllUtilSClass a = new AllUtilSClass();
        AllUtilSClass.OrderDetailsViewHolder cvh = a.new OrderDetailsViewHolder();

        cvh.imv_product_image = (ImageView) v.findViewById(R.id.imv_product_image);

        AllUtilMethods.getImage(OrderDetailsActivity.this
                , R.drawable.subcategory_placeholder
                , my_order_details.al_order_items.get(position).image_url
                , cvh.imv_product_image, 200, 200);


        cvh.tv_title = (TextView) v.findViewById(R.id.tv_title);
        cvh.tv_title.setText("" + my_order_details.al_order_items.get(position).productName);
        cvh.tv_title.setSelected(true);

        cvh.tv_qty = (TextView) v.findViewById(R.id.tv_qty);
        cvh.tv_qty.setText(" QTY: " + my_order_details.al_order_items.get(position).qty);

        cvh.tv_price = (TextView) v.findViewById(R.id.tv_price);
        cvh.tv_price.setText("" + my_order_details.al_order_items.get(position).price);

        cvh.ll_holder=(LinearLayout)v.findViewById(R.id.ll_holder);
        cvh.ll_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(OrderDetailsActivity.this, ProductDetailsActivity.class));
                intent.putExtra("cat_id", my_order_details.al_order_items.get(position).productId);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        al_viewholder.add(cvh);
        return v;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        AllUtilMethods.disableMenuItemForSometimes(item);
        switch (item.getItemId()) {
            case R.id.action_home:
                intent = new Intent(new Intent(OrderDetailsActivity.this, NonHomePage.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);

                break;
            case R.id.action_search:
                intent = new Intent(new Intent(OrderDetailsActivity.this, SearchActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            //      case R.id.action_cart:
//                intent = new Intent(new Intent(OrderDetailsActivity.this, MyCartActivity.class));
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            //          break;
            case R.id.action_account:
                intent = new Intent(new Intent(OrderDetailsActivity.this, MyAccount.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_contact_us:
                intent = new Intent(new Intent(OrderDetailsActivity.this, WebViewActivity.class));
                intent.putExtra("page", "con");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_about_us:
                intent = new Intent(new Intent(OrderDetailsActivity.this, WebViewActivity.class));
                intent.putExtra("page", "ab");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_logout:
                intent = new Intent(new Intent(OrderDetailsActivity.this, NonLoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                getSharedPreferences("login", 0).edit().clear().commit();
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_order:
                intent = new Intent(new Intent(OrderDetailsActivity.this, TrackYourOrderActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_change_password:
                AllUtilMethods.changePasswordDialog(OrderDetailsActivity.this, overview_coordinator_layout);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel_order:

                break;
        }
    }

    public void getOrderItemList(String cust_id, String order_id) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", cust_id);
        map.put("orderid", order_id);

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                my_order_details = AllUtilMethods.parseMyOrderHistory(result);
                if (my_order_details != null) {
                    setUI();
                } else {
                    finish();
                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
                finish();
            }
        }, OrderDetailsActivity.this).performPostRequest(OrderDetailsActivity.this, AllUtilUrls.base_url + "" + AllUtilUrls.order_details, map, "Please wait ...", 0);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }
}
