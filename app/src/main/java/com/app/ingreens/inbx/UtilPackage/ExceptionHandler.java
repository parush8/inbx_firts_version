package com.app.ingreens.inbx.UtilPackage;

import android.app.Activity;
import android.content.Intent;

import com.app.ingreens.inbx.UncaughtExceptionHandlerActivity;

/**
 * Created by root on 9/13/16.
 */
public class ExceptionHandler implements
        java.lang.Thread.UncaughtExceptionHandler {
    private final Activity myContext;
    private final String LINE_SEPARATOR = "\n";

    public ExceptionHandler(Activity context) {
        myContext = context;
    }

    public void uncaughtException(Thread thread, Throwable exception) {


        Intent intent = new Intent(myContext, UncaughtExceptionHandlerActivity.class);
        myContext.startActivity(intent);

        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(10);
        System.out.println(" EXCEPTION "+exception);
    }

}