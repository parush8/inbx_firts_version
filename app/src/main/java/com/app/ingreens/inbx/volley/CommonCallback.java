package com.app.ingreens.inbx.volley;

public interface CommonCallback {
	
	public void onNetworkProcessingCompleted(String result);
	public void onErrorEncountered(String msg);

}
