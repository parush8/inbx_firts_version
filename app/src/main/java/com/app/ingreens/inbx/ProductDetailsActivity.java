package com.app.ingreens.inbx;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ingreens.inbx.NonLoginActivity;
import com.app.ingreens.inbx.R;
import com.app.ingreens.inbx.UtilAdapters.ProductDetailsItemImageAdpter;
import com.app.ingreens.inbx.UtilAdapters.ProductDetailsSimilarProductImageAdapter;
import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.AllUtilSClass;
import com.app.ingreens.inbx.UtilPackage.AllUtilUrls;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.UtilPackage.MyTagHandler;
import com.app.ingreens.inbx.volley.CommonCallback;
import com.app.ingreens.inbx.volley.GeneralNetworkRequestClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Papun's on 7/23/2016.
 */
public class ProductDetailsActivity extends MyCustomActivity implements View.OnClickListener, ProductDetailsItemImageAdpter.OnImageClickCallback {
    //////////////////// UI VARIABLES ///////////////////
    private Toolbar toolbar;
    private TextView toolbar_title, tv_title, tv_price, tv_desc, tv_rgprice, tv_tier_price;
    private TextView tv_similar_product_lable;
    private EditText tv_count;
    private ImageButton btn_minus, btn_plus;
    private Button btn_add_to_cart;
    private CoordinatorLayout overview_coordinator_layout;
    private RecyclerView rv_product_image, rv_similar_product_image;
    private AllUtilSClass.ProductDetails pro_details;
    private ImageView imv_product_details;
    private ImageView imv_cart;
    private TextView tv_cart_count;
    /////////////////////////////////////////////////////
    //////////// NORMAL VARIABLES ///////////////////////
    boolean resume;
    private int cart_count = 0; // THIS VARIABLE IS FOR COUNTING THE NUMBER OF IS BEING ADDED
    private String main_img = "";

    //////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(ProductDetailsActivity.this));
        setContentView(R.layout.product_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);

        imv_cart = (ImageView) toolbar.findViewById(R.id.imv_cart);
        imv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(getSharedPreferences("login", 0).getString("cart_count", "0")) > 0) {
                    Intent intent = new Intent(new Intent(ProductDetailsActivity.this, MyCartActivity.class));
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.stay);
                } else {
                    AllUtilMethods.emptyCartDialog(ProductDetailsActivity.this);
                }
            }
        });

        imv_product_details = (ImageView) findViewById(R.id.imv_product_details);
        imv_product_details.setEnabled(true);

        tv_cart_count = (TextView) toolbar.findViewById(R.id.tv_global_cart_count);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            overview_coordinator_layout = (CoordinatorLayout) findViewById(R.id.overview_coordinator_layout);
            overview_coordinator_layout.setVisibility(View.INVISIBLE);
            getProductDetails(getSharedPreferences("login", 0).getString("customer_id", "0"), getIntent().getStringExtra("cat_id"));
        }
        imv_product_details.setEnabled(true);
        resume = true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }

    private void setUI() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));

        ImageView imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(ProductDetailsActivity.this, NonHomePage.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
            }
        });
        setToolbarTitle("" + pro_details.name);

        btn_add_to_cart = (Button) findViewById(R.id.btn_add_to_cart);
        if (pro_details.stock_status.equalsIgnoreCase("1")) {
            btn_add_to_cart.setText("Add to Cart");
            btn_add_to_cart.setEnabled(true);
        } else {
            btn_add_to_cart.setText("Out of Stock");
            btn_add_to_cart.setEnabled(false);
        }
        btn_add_to_cart.setOnClickListener(this);

        rv_similar_product_image = (RecyclerView) findViewById(R.id.rv_similar_product_image);
        rv_product_image = (RecyclerView) findViewById(R.id.rv_product_image);
        ProductDetailsItemImageAdpter ama = new ProductDetailsItemImageAdpter(ProductDetailsActivity.this, pro_details.al_image_url, ProductDetailsActivity.this);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(ProductDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rv_product_image.setLayoutManager(horizontalLayoutManagaer);
        rv_product_image.setAdapter(ama);

        ProductDetailsSimilarProductImageAdapter amp = new ProductDetailsSimilarProductImageAdapter(ProductDetailsActivity.this, pro_details.al_similar_product);
        rv_similar_product_image.setAdapter(amp);

        tv_count = (EditText) findViewById(R.id.tv_count);

        btn_minus = (ImageButton) findViewById(R.id.btn_minus);
        btn_minus.setOnClickListener(this);

        btn_plus = (ImageButton) findViewById(R.id.btn_plus);
        btn_plus.setOnClickListener(this);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("" + pro_details.name);

        tv_price = (TextView) findViewById(R.id.tv_price);
        tv_rgprice = (TextView) findViewById(R.id.tv_rgprice);
        if (pro_details.spprice.length() > 3) {
            tv_price.setText("" + pro_details.spprice);
            tv_rgprice.setVisibility(View.VISIBLE);
            tv_rgprice.setText("M.R.P. " + pro_details.rgprice);
            tv_rgprice.setPaintFlags(tv_rgprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            tv_price.setText("" + pro_details.rgprice);
            tv_rgprice.setVisibility(View.GONE);
        }

        tv_desc = (TextView) findViewById(R.id.tv_desc);
        tv_desc.setText(Html.fromHtml(pro_details.description, null, new MyTagHandler()));

        imv_product_details.setOnClickListener(this);
        main_img = pro_details.image_url;
        AllUtilMethods.getImage(ProductDetailsActivity.this, R.drawable.category_placeholder, main_img, imv_product_details, 550, 650);

        tv_similar_product_lable = (TextView) findViewById(R.id.tv_similar_product_lable);
        if (pro_details.al_similar_product.size() > 0) {
            tv_similar_product_lable.setVisibility(View.VISIBLE);
        } else {
            tv_similar_product_lable.setVisibility(View.GONE);
        }

        tv_tier_price = (TextView) findViewById(R.id.tv_tier_price);
        System.out.println(" sdfsdfs  " + pro_details.al_tier_price.size());
        if (pro_details.al_tier_price.size() > 0) {
            String temp_str = "";
            for (int i = 0; i < pro_details.al_tier_price.size(); i++) {
                temp_str = temp_str + pro_details.al_tier_price.get(i).info + "\n";
            }
            tv_tier_price.setText(temp_str);
            tv_tier_price.setVisibility(View.VISIBLE);
        } else {
            tv_tier_price.setVisibility(View.GONE);
        }
        overview_coordinator_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        AllUtilMethods.disableMenuItemForSometimes(item);
        switch (item.getItemId()) {
            case R.id.action_home:
                intent = new Intent(new Intent(ProductDetailsActivity.this, NonHomePage.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);

                break;
            case R.id.action_search:
                intent = new Intent(new Intent(ProductDetailsActivity.this, SearchActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            //      case R.id.action_cart:
//                intent = new Intent(new Intent(ProductDetailsActivity.this, MyCartActivity.class));
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            case R.id.action_account:
                intent = new Intent(new Intent(ProductDetailsActivity.this, MyAccount.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_contact_us:
                intent = new Intent(new Intent(ProductDetailsActivity.this, WebViewActivity.class));
                intent.putExtra("page", "con");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_about_us:
                intent = new Intent(new Intent(ProductDetailsActivity.this, WebViewActivity.class));
                intent.putExtra("page", "ab");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_logout:
                intent = new Intent(new Intent(ProductDetailsActivity.this, NonLoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                getSharedPreferences("login", 0).edit().clear().commit();
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_order:
                intent = new Intent(new Intent(ProductDetailsActivity.this, TrackYourOrderActivity.class));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.action_change_password:
                AllUtilMethods.changePasswordDialog(ProductDetailsActivity.this, overview_coordinator_layout);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbarTitle(String msg) {
        toolbar_title.setText("" + msg);
        toolbar.setSelected(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        cart_count=Integer.parseInt(tv_count.getText().toString().trim());
        System.out.println(" CART COUNT "+cart_count);
        switch (v.getId()) {
            case R.id.btn_add_to_cart:
                cart_count=Integer.parseInt(tv_count.getText().toString().trim());
                if (cart_count > 0) {
                    AllUtilMethods.addToCart(ProductDetailsActivity.this,
                            getSharedPreferences("login", 0).getString("shoppingcartid", "")
                            , getSharedPreferences("login", 0).getString("customer_id", "")
                            , pro_details.product_id, "" + cart_count,
                            overview_coordinator_layout, 1);
                } else {
                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Select items to add cart.");
                }

                break;
            case R.id.btn_minus:
                if (cart_count > 0) {
                    cart_count--;
                    setItemCount(cart_count);
                }
                break;
            case R.id.btn_plus:
                if(cart_count<AllUtilUrls.MAX_CART_NUMBER){
                    cart_count++;
                    setItemCount(cart_count);
                }
                break;
            case R.id.imv_product_details:
                imv_product_details.setEnabled(false);
                ImagePinchActivity.photo = ((BitmapDrawable) imv_product_details.getDrawable()).getBitmap();
                System.out.println("  main url " + main_img);
                intent = new Intent(new Intent(ProductDetailsActivity.this, ImagePinchActivity.class));
                intent.putExtra("product_name",pro_details.name );
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
        }


        /////////////////////// THIS BELOW SEGMENT IS ONLY FOR FRAGMENT HANDLING ????????????????????????????????????????
    }

    private void setItemCount(int cart_count) {
        tv_count.setText("" + cart_count);
        tv_count.setSelection(tv_count.getText().length());
    }

    public void getProductDetails(String cust_id, String pid) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("customer_id", cust_id);
        map.put("pid", "" + pid);

        new GeneralNetworkRequestClass(new CommonCallback() {
            @Override
            public void onNetworkProcessingCompleted(String result) {
                pro_details = AllUtilMethods.parseProductDetails(result);
                setUI();
//                try {
//                    Log.e(" JSON ", result);
//                    JSONObject status = new JSONObject(result);
//                    JSONObject pdata = status.getJSONObject("pdata");
//                    if (status.getString("success").equalsIgnoreCase("true")) {
//
//                    } else {
//                        AllUtilMethods.CreateSnackBar(overview_coordinator_layout, pdata.getString("errMsg"));
//                    }
//                } catch (JSONException e) {
//                    System.out.println(" JSON EXCEPTION " + e);
//                    AllUtilMethods.CreateSnackBar(overview_coordinator_layout, "Sorry! Data retrieve error has occurred, please try later.");
//                }
            }

            @Override
            public void onErrorEncountered(String msg) {
                AllUtilMethods.CreateSnackBar(overview_coordinator_layout, msg);
            }
        }, ProductDetailsActivity.this).performPostRequest(ProductDetailsActivity.this, AllUtilUrls.base_url + "" + AllUtilUrls.product_details, map, "Please wait ...", 0);
    }

    @Override
    public void onImageClick(String url) {
        main_img = url;
        AllUtilMethods.getImage(ProductDetailsActivity.this, R.drawable.category_placeholder, main_img, imv_product_details, 800, 800);

    }

    @Override
    public void onCartItemUpdate() {
        super.onCartItemUpdate();
        tv_cart_count.setText("" + getSharedPreferences("login", 0).getString("cart_count", "0"));
    }
}
