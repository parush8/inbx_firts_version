package com.app.ingreens.inbx;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.app.ingreens.inbx.UtilPackage.AllUtilMethods;
import com.app.ingreens.inbx.UtilPackage.ExceptionHandler;
import com.app.ingreens.inbx.UtilPackage.SandboxView;

/**
 * Created by Papun's on 7/23/2016.
 */
public class ImagePinchActivity extends Activity {

    boolean resume;
    private View sandbox;
    private TextView tv_close;
    private FrameLayout fm_photo;
    private Button btn_share;
    private String product_name;
    public static Bitmap photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(ImagePinchActivity.this));
        setContentView(R.layout.image_pinch_activity);

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            setUI();
        }
        resume = true;
    }

    private void setUI() {
        fm_photo = (FrameLayout) findViewById(R.id.fm_photo);
        product_name=getIntent().getExtras().getString("product_name");
        sandbox = new SandboxView(ImagePinchActivity.this, photo);
        fm_photo.addView(sandbox);

        tv_close = (TextView) findViewById(R.id.tv_close);
        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                sandbox = null;
                photo = null;
                overridePendingTransition(R.anim.stay, R.anim.slide_down);
            }
        });

        btn_share = (Button) findViewById(R.id.btn_share);
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(" PRODUCT NAME "+product_name);
                AllUtilMethods.shareBitmap(ImagePinchActivity.this, photo, "" + System.currentTimeMillis(),product_name);
            }
        });
    }
}
